#include <iostream>
#include <cstdint>
#include <cmath>

class FixedPoint2
{
private:
	std::int_least16_t m_base{};
	std::int_least8_t m_decimal{};

public:
	//default constructor
	FixedPoint2(std::int_least16_t base=0, std::int_least8_t decimal=0)
		:m_base{ base }, m_decimal{ decimal }
	{
		if (m_base < 0 || m_decimal < 0)
		{
			//make sure base is negative
			if (m_base > 0)
				m_base = -m_base;
			//make sure decimal is negative
			if (m_decimal > 0)
				m_decimal = -m_decimal;
		}
	}
	
	//double type input constructor
	FixedPoint2(double input)
	{
		m_base = static_cast<std::int_least16_t>(input);
		m_decimal = static_cast<std::int_least8_t>(round((input - m_base)*100));
	}

	operator double() const
	{
		return m_base + static_cast<double>(m_decimal) / 100.0;
	}

	friend std::istream& operator>>(std::istream& in, FixedPoint2& f);
	friend bool operator==(const FixedPoint2& f1, const FixedPoint2& f2);
};

FixedPoint2 operator+(const FixedPoint2& f1, const FixedPoint2& f2)
{
	return FixedPoint2(static_cast<double>(f1) + static_cast<double>(f2));
}

bool operator==(const FixedPoint2& f1, const FixedPoint2& f2)
{
	return(f1.m_base == f2.m_base && f1.m_decimal == f2.m_decimal);
}

std::ostream& operator<<(std::ostream& out, const FixedPoint2& f)
{
	out << static_cast<double>(f);
	return out;
}

std::istream& operator>>(std::istream& in, FixedPoint2& f)
{
	double d{};
	in >> d;
	f = FixedPoint2(d);
	return in;
}

//below is test, however cannot open them all together, need to seperate testing by commenting
void test1()
{
	FixedPoint2 a{ 34,56 };
	std::cout << a << '\n';

	FixedPoint2 b{ -2, 8 };
	std::cout << b << '\n';

	FixedPoint2 c{ 2, -8 };
	std::cout << c << '\n';

	FixedPoint2 d{ -2, -8 };
	std::cout << d << '\n';

	FixedPoint2 e{ 0, -5 };
	std::cout << e << '\n';

	std::cout << static_cast<double>(e) << '\n';
}

void test2()
{
	FixedPoint2 a{ 0.01 };
	std::cout << a << '\n';

	FixedPoint2 b{ -0.01 };
	std::cout << b << '\n';

	FixedPoint2 c{ 5.01 }; // stored as 5.0099999... so we'll need to round this
	std::cout << c << '\n';

	FixedPoint2 d{ -5.01 }; // stored as -5.0099999... so we'll need to round this
	std::cout << d << '\n';
}

void testAddition()
{
	// h/t to reader Sharjeel Safdar for this function
	std::cout << std::boolalpha;
	std::cout << (FixedPoint2{ 0.75 } +FixedPoint2{ 1.23 } == FixedPoint2{ 1.98 }) << '\n'; // both positive, no decimal overflow
	std::cout << (FixedPoint2{ 0.75 } +FixedPoint2{ 1.50 } == FixedPoint2{ 2.25 }) << '\n'; // both positive, with decimal overflow
	std::cout << (FixedPoint2{ -0.75 } +FixedPoint2{ -1.23 } == FixedPoint2{ -1.98 }) << '\n'; // both negative, no decimal overflow
	std::cout << (FixedPoint2{ -0.75 } +FixedPoint2{ -1.50 } == FixedPoint2{ -2.25 }) << '\n'; // both negative, with decimal overflow
	std::cout << (FixedPoint2{ 0.75 } +FixedPoint2{ -1.23 } == FixedPoint2{ -0.48 }) << '\n'; // second negative, no decimal overflow
	std::cout << (FixedPoint2{ 0.75 } +FixedPoint2{ -1.50 } == FixedPoint2{ -0.75 }) << '\n'; // second negative, possible decimal overflow
	std::cout << (FixedPoint2{ -0.75 } +FixedPoint2{ 1.23 } == FixedPoint2{ 0.48 }) << '\n'; // first negative, no decimal overflow
	std::cout << (FixedPoint2{ -0.75 } +FixedPoint2{ 1.50 } == FixedPoint2{ 0.75 }) << '\n'; // first negative, possible decimal overflow
}

void testInput()
{
	FixedPoint2 a{ -0.48 };
	std::cout << a << '\n';

	std::cout << -a << '\n';

	std::cout << "Enter a number: "; // enter 5.678
	std::cin >> a;

	std::cout << "You entered: " << a << '\n';
}

int main()
{
	//test1();
	test2();
	//testAddition();
	//testInput();
	
	return 0;
}