#include <iostream>

class Average
{
private:
	std::int_least32_t m_sum;
	std::int_least8_t m_number;

public:
	Average(std::int_least32_t sum = 0, std::int_fast8_t number=0)
		:m_sum{ sum }, m_number{ number }
	{

	}

	Average& operator+=(int value);

	friend std::ostream& operator<<(std::ostream& out, const Average& average);
};

Average& Average::operator+=(int value)
{
	m_sum += value;
	++m_number;
	return *this;
}


std::ostream& operator<<(std::ostream& out, const Average& average)
{
	out << (static_cast<double>(average.m_sum) / static_cast<double>(average.m_number));
	return out;
}

int main()
{
	Average avg{};

	avg += 4;
	std::cout << avg << '\n';

	avg += 8;
	std::cout << avg << '\n';

	avg += 24;
	std::cout << avg << '\n';

	avg += -10;
	std::cout << avg << '\n';

	(avg += 6) += 10;
	std::cout << avg << '\n';

	Average copy{ avg };
	std::cout << copy << '\n';

	return 0;
}