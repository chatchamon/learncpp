#include <iostream>
#include <cassert>

class IntArray
{
private:
	int m_length{ 0 };
	int* m_array{ nullptr };

public:
	IntArray(int length)
		:m_length{ length }
	{
		assert(length > 0);
		m_array = new int[length] {};
	}

	//copy constructor
	IntArray(const IntArray& array)
		:m_length{ array.m_length }
	{
		m_array = new int[array.m_length];
		for (int i{ 0 }; i < array.m_length; ++i)
			m_array[i] = array.m_array[i];
	}

	//destructor
	~IntArray()
	{
		delete[] m_array;
	}

	int& operator[](int index)
	{
		assert(index >= 0);
		assert(index <= m_length);
		return m_array[index];
	}

	IntArray& operator=(IntArray& array)
	{
		//prevent self-assignment
		if (this == &array)
			return *this;

		delete[] m_array;

		m_length = array.m_length;
		m_array = new int[m_length];
		for (int i{ 0 }; i < m_length; ++i)
			m_array[i] = array.m_array[i];

		//return *this;
	}

	friend std::ostream& operator<<(std::ostream& out, const IntArray& array)
	{
		for (int i{ 0 }; i < array.m_length; ++i)
			out << array.m_array[i] << ' ';
		return out;
	}
};

IntArray fillArray()
{
	IntArray a(5);

	a[0] = 5;
	a[1] = 8;
	a[2] = 2;
	a[3] = 3;
	a[4] = 6;

	return a;
}

int main()
{
	IntArray a{ fillArray() };
	std::cout << a << '\n';

	auto& ref{ a };
	a = ref;
	//std::cout << "a: " << a << '\n';
	//std::cout << "ref: " << ref << '\n';

	IntArray b(1);
	b = a;
	std::cout << "b: " << b << '\n';

	return 0;
}
