#include <cassert>
#include <iostream>

class Fraction
{
private:
	int m_numerator;
	int m_denominator;
public:
	Fraction(int num = 0, int den = 1)
		:m_numerator{ num }, m_denominator{ den }
	{
		assert(m_denominator != 0);
	}

	Fraction(const Fraction& copy)
		:m_numerator{ copy.m_numerator }, m_denominator{ copy.m_denominator }
	{
		std::cout << "Copy constructor called\n";
	}

	Fraction& operator=(const Fraction& fraction);
	friend std::ostream& operator<<(std::ostream& out, const Fraction& fraction);
};

std::ostream& operator<<(std::ostream& out, const Fraction& fraction)
{
	out << fraction.m_numerator << "/" << fraction.m_denominator;
	return out;
}

Fraction& Fraction::operator=(const Fraction& fraction)
{
	m_numerator = fraction.m_numerator;
	m_denominator = fraction.m_denominator;
	return *this;
}

int main()
{
	Fraction fiveThird(5, 3);
	Fraction f;
	f = fiveThird;
	std::cout << f;
	std::cout << '\n';
	
	Fraction f1(2, 6);
	Fraction f2(4, 9);
	Fraction f3(7, 8);
	std::cout << f1 << " " << f2 << " " << f3 << '\n';

	f1 = f2 = f3;
	std::cout << f1 << " " << f2 << " " << f3 << '\n';

	return 0;
}