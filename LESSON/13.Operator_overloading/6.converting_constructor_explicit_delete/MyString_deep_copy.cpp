#include <iostream>
#include <cassert>
#include <cstring>	// for strlen()

class MyString
{
private:
	char *m_data;
	int m_length;

public:
	MyString(const char* source = "")
	{
		//make sure source is not a null ptr
		assert(source);

		//make length +1 for a terminator
		m_length = std::strlen(source) + 1;

		//allocate a buffer equal to this length
		m_data = new char[m_length];

		//copy the parameter string into the internal buffer
		for (int i{ 0 }; i < m_length; ++i)
			m_data[i] = source[i];

		//make sure the string is terminated
		m_data[m_length - 1] = '\0';
	}

	void deepCopy(const MyString& source);

	~MyString() //destructor
	{
		delete[] m_data;
	}

	char* getString() { return m_data; }
	int getLength() { return m_length; }

	MyString& operator=(const MyString& source);


};

void MyString::deepCopy(const MyString& source)
{
	//deallocate any value that this string is holding
	delete[] m_data;

	m_length = source.m_length;

	if (source.m_data)
	{
		m_data = new char[m_length];

		for (int i{ 0 }; i < m_length; ++i)
			m_data[i] = source.m_data[i];
	}
	else
		m_data = nullptr;
}

MyString::MyString(const MyString& source) :
	m_data{ nullptr }
{
	deepCopy(source);
}

MyString& MyString::operator=(const MyString& source)
{
	if (this == source)
		return *this;

	deepCopy(source);

	return *this;
}