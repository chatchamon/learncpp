#include <iostream>
#include <string>

class MyString
{
private:
	std::string m_string;

	MyString(char) = delete;	// any use of this constructor is an error

public:
	// explicit keyword makes this constructor ineligible for implicit conversions
	explicit MyString(int x)
	{
		m_string.resize(x);
	}

	MyString(const char* string)
	{
		m_string = string;
	}
	
	friend std::ostream& operator<<(std::ostream& out, const MyString& s);
};

std::ostream& operator<<(std::ostream& out, const MyString& s)
{
	out << s.m_string;
	return out;
}

void printString(const MyString& s)
{
	std::cout << s;
}

int main()
{
	MyString mine = 'x';  // compile error, since MyString(char) is deleted
	std::cout << mine;

	printString('x');
	return 0;
}