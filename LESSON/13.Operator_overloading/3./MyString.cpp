#include <string>
#include <iostream>

class MyString
{
private:
	std::string m_string{};

public:
	MyString(std::string string)
		:m_string{ string }
	{}

	std::string operator()(int index, int length)
	{
		std::string pr{};
		for (int i{ 0 }; i < length; ++i)
			pr += m_string[static_cast<std::string::size_type>(i + index)];

		return pr;
	}
};

int main()
{
	MyString string{ "Hello, World!" };
	std::cout << string(7, 5) << '\n';

	return 0;
}
