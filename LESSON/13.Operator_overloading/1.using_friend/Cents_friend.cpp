#include <iostream>

class Cents
{
private:
	int m_cents{0};

public:
	Cents(int cents) { m_cents = cents; }

	friend Cents operator+(const Cents& c1, const Cents& c2);
	friend Cents operator+(const Cents& c1, int value);

	friend Cents operator-(const Cents& c1, const Cents& c2);

	int getCents() { return m_cents; }
};

Cents operator+(const Cents& c1, const Cents& c2)
{
	return (c1.m_cents + c2.m_cents);
}

Cents operator-(const Cents& c1, const Cents& c2)
{
	return (c1.m_cents - c2.m_cents);
}

int main()
{
	Cents c1{ 6 };
	Cents c2{ 8 };
	Cents sum{ c1 + c2 };
	Cents minus{ c1 - c2 };

	std::cout << c1.getCents() << " + " << c2.getCents() << " = " << sum.getCents() << '\n';
	std::cout << c1.getCents() << " - " << c2.getCents() << " = " << minus.getCents() << '\n';
}