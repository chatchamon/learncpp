#include <iostream>

class Cents
{
private:
	int m_cents{ 0 };

public:
	Cents(int cents) { m_cents = cents; }

	friend Cents operator+(const Cents& c1, int value);
	friend Cents operator+(int value, const Cents& c1);

	int getCents() const { return m_cents; }
};

Cents operator+(const Cents& c1, int value)
{
	return (c1.m_cents + value);
}
Cents operator+(int value, const Cents& c1)
{
	return (value + c1.m_cents);
}

int main()
{
	Cents c1 = Cents(4) + 20;
	Cents c2 = 21 + Cents(4);

	std::cout << c1.getCents() << '\n';
	std::cout << c2.getCents() << '\n';
}