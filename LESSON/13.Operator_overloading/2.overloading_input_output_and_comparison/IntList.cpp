#include <iostream>
#include <cassert>

class IntList
{
private:
	int m_list[10]{0,1,2,3,4,5,6,7,8,9};

public:
	int& operator[](int index);
	const int& operator[](int index) const;
};

int& IntList::operator[](int index)
{
	assert(index >= 0 && index < 10);
	return m_list[index];
}

const int& IntList::operator[](int index) const
{
	return m_list[index];
}

int main()
{
	IntList list{};
	list[2] = 3;
	std::cout << list[2] << '\n';

	//IntList list1{};
	//list[12] = 12;
	//std::cout << list1[12] << '\n';

	//const IntList clist{};
	//clist[2] = 3;		// compile error: calls const version of operator[], which returns a const reference.  Cannot assign to this.
	//std::cout << clist << '\n';

	IntList *list2{ new IntList{} };
	(*list2)[3] = 9;
	delete list2;

	return 0;
}