#include <iostream>
#include <map>
#include <string>

int main()
{
	std::map<std::string, char> grades{
		{"Joe", 'A'},
		{"Frank", 'B'}
	};

	grades["Susan"] = 'C';
	grades["Tom"] = 'D';

	std::cout << "Joe has a grade of " << grades["Joe"] << '\n';
	std::cout << "Frank has a grade of " << grades["Frank"] << '\n';
	std::cout << "Susan has a grade of " << grades["Susan"] << '\n';
	std::cout << "Tom has a grade of " << grades["Tom"] << '\n';

	return 0;
}