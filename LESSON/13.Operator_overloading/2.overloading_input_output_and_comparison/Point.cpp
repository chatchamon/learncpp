#include <iostream>

class Point
{
private:
	double m_x;
	double m_y;
	double m_z;

public:
	Point(double x=0.0, double y=0.0, double z=0.0)
		:m_x{ x }, m_y{ y }, m_z{ z }
	{
	}

	friend std::ostream& operator<<(std::ostream& out, const Point& point);
	friend std::istream& operator>>(std::istream& in, Point &point);

	Point operator-() const;
	Point operator+() const;
	bool operator!() const;
};

std::ostream& operator<<(std::ostream& out, const Point& point)
{
	out << "Point(" << point.m_x << ", " << point.m_y << ", " << point.m_z << ")";
	return out;
}

std::istream& operator>>(std::istream& in, Point &point)
{
	in >> point.m_x;
	in >> point.m_y;
	in >> point.m_z;

	return in;
}

Point Point::operator-() const
{
	return Point(-m_x, -m_y, -m_z);
}

Point Point::operator+() const
{
	return Point(m_x, m_y, m_z);
	// or use
	// return *this;
}

bool Point::operator!() const
{
	return (m_x == 0.0 && m_y == 0.0 && m_z == 0.0);
}

int main()
{
	std::cout << "Enter a point: ";
	Point p1;
	std::cin >> p1;

	if (!p1)
		std::cout << "point is set at the origin.\n";
	else
		std::cout << "point is not set at the origin.\n";

	std::cout << -p1;

	return 0;

	std::cout << p1 << '\n';
}