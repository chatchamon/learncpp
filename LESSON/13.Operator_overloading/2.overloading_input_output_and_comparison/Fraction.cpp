#include <iostream>
#include <cassert>

class Fraction
{
private:
	int m_numerator{ 0 };
	int m_denomenator{ 1 };

public:
	Fraction(int num = 0, int den = 1)
		: m_numerator{ num }, m_denomenator{ den }
	{
		reduce();
	}

	static int gcd(int a, int b) {
		return (b == 0) ? (a > 0 ? a : -a) : gcd(b, a % b);
	}

	void reduce()
	{
		if (m_numerator != 0 && m_denomenator != 0)
		{
			int gcd = Fraction::gcd(m_numerator, m_denomenator);
			m_numerator /= gcd;
			m_denomenator /= gcd;
		}
	}

	friend std::ostream& operator<<(std::ostream& out, const Fraction& fraction);
	friend std::istream& operator>>(std::istream& in, Fraction& fraction);

	friend Fraction operator*(const Fraction& f1, const Fraction& f2);
	friend Fraction operator*(const Fraction& f, int value);
	friend Fraction operator*(int value, const Fraction& f);

	//void print() const
	//{
		//std::cout << m_numerator << "/" << m_denomenator << '\n';
	//}
};

std::ostream& operator<<(std::ostream& out, const Fraction& fraction)
{
	out << fraction.m_numerator << "/" << fraction.m_denomenator;
	return out;
}

std::istream& operator>>(std::istream& in, Fraction& fraction)
{
	in >> fraction.m_numerator;
	//in.ignore(32767, '\n');			// in case you wait for next line number
	in >> fraction.m_denomenator;
	fraction.reduce();

	return in;
}

Fraction operator*(const Fraction& f1, const Fraction& f2)
{
	return Fraction(f1.m_numerator * f2.m_numerator, f1.m_denomenator * f2.m_denomenator);
}

Fraction operator*(const Fraction& f, int value)
{
	return Fraction(f.m_numerator * value, f.m_denomenator);
}

Fraction operator*(int value, const Fraction& f)
{
	return Fraction(f*value);
}

int main()
{
	Fraction f1;
	std::cout << "Enter fraction 1: ";
	std::cin >> f1;
	std::cout << f1 << '\n';

	/*
	Fraction f1(2, 5);
	f1.print();

	Fraction f2(3, 8);
	f2.print();

	Fraction f3 = f1 * f2;
	f3.print();

	Fraction f4 = f1 * 2;
	f4.print();

	Fraction f5 = 2 * f2;
	f5.print();

	Fraction f6 = Fraction(1, 2) * Fraction(2, 3) * Fraction(3, 4);
	f6.print();
	*/
	

	return 0;
}