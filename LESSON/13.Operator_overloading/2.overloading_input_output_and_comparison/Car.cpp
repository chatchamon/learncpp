#include <iostream>
#include <algorithm>
#include <string>
#include <vector>

class Car
{
private:
	std::string m_brand;
	std::string m_model;

public:
	Car(std::string brand, std::string model)
		:m_brand{ brand }, m_model{model}
	{}

	friend bool operator==(const Car& c1, const Car& c2);
	friend bool operator!=(const Car& c1, const Car& c2);
	friend std::ostream& operator<<(std::ostream& out, const Car c);
	friend bool operator<(const Car& c1, const Car& c2);
};

bool operator==(const Car& c1, const Car& c2)
{
	return ((c1.m_brand == c2.m_brand) && (c1.m_model == c2.m_model));
}

bool operator!=(const Car& c1, const Car& c2)
{
	return !(c1 == c2);
}

std::ostream& operator<<(std::ostream& out, const Car c)
{
	out << "(" << c.m_brand << ", " << c.m_model << ")";
	return out;
}

bool operator<(const Car& c1, const Car& c2)
{
	if (c1.m_brand == c2.m_brand)
		return c1.m_model < c2.m_model;
	else
		return c1.m_brand < c2.m_brand;
}

int main()
{
	std::vector<Car> v{
	{"Toyota", "Corolla"},
	{"Honda", "Accord"},
	{"Toyota", "Camry"},
	{"Honda", "Civic"}
	};

	std::sort(v.begin(), v.end()); // requires an overloaded Car::operator<

	for (const auto& car : v)
		std::cout << car << '\n';

	return 0;
}