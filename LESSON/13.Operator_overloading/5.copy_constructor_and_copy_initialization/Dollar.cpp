#include <iostream>

class Cents
{
private:
	int m_cents;

public:
	Cents(int cents=0) 
		:m_cents{cents}
	{}

	// Overloaded int cast
	operator int() const { return m_cents; }

	int getCents() { return m_cents; }
	int setCents(int cents) { m_cents = cents; }
};

class Dollar
{
private:
	int m_dollars;

public:
	Dollar(int dollar = 0)
		:m_dollars{ dollar }
	{}

	operator Cents() const { return m_dollars * 100; }
};

void printCents(Cents cents)
{
	std::cout << cents;
}

int main()
{
	Dollar dollar{ 9 };
	printCents(dollar);

	std::cout << '\n';

	return 0;
}