#include <iostream>
#include <cassert>

class Fraction
{
private:
	int m_numerator;
	int m_denominator;

public:
	Fraction(int num, int den)
		:m_numerator{ num }, m_denominator{ den }
	{
		assert(den != 0);
	}


	Fraction(const Fraction& fraction)
		:m_numerator{ fraction.m_numerator }, m_denominator{ fraction.m_denominator }
	{
		std::cout << "Copy constructor is called\n";
	}

	friend std::ostream& operator<<(std::ostream& out, const Fraction& fraction);
	int getNumerator() { return m_numerator; }
	void setNumerator(int numerator) { m_numerator = numerator; }
};

std::ostream& operator<<(std::ostream& out, const Fraction& fraction)
{
	out << fraction.m_numerator << '/' << fraction.m_denominator;
	return out;
}

void printFraction(const Fraction& f)
{
	std::cout << f;
}


Fraction makeNegative(Fraction f) // ideally we should do this by const reference
{
	f.setNumerator(-f.getNumerator());
	return f;
}

int main()
{
	//Fraction fiveThird(Fraction(5, 3));
	//Fraction fCopy = fiveThird;		// copy is protected
	//std::cout << fiveThird << '\n';

	//Fraction fiveThirds(5, 3);
	//std::cout << makeNegative(fiveThirds);

	//printFraction(6);

	return 0;
}