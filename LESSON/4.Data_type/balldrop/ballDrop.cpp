#include <iostream>
#include "constant.h"

double getTowerHeight()
{
	std::cout << "Enter the height of the tower: ";
	double towerHeight{};
	std::cin >> towerHeight;
	return towerHeight;
}

double distanceFallen(double towerHeight, int second)
{
	double distance_fallen{};
	distance_fallen = towerHeight - (gravity * (second * second) / 2.0);		//do not forget 2.0 as we deal with double
	return distance_fallen;
}

void printDistance(double towerHeight, int second)
{
	if (second < 5) {
		std::cout << "At " << second << " seconds, the ball is at height: " << distanceFallen(towerHeight, second) << " meters";
		std::cout << std::endl;
	}
	else if (second == 5)
		std::cout << "At " << second << " seconds, the ball is on the ground\n";
}

int main()
{
	double h{ getTowerHeight() };
	printDistance(h, 0);
	printDistance(h, 1);
	printDistance(h, 2);
	printDistance(h, 3);
	printDistance(h, 4);
	printDistance(h, 5);

	return 0;
}