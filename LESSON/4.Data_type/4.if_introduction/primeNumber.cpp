#include <iostream>

bool isPrime(int input)
{
	if (input == 2)
		return true;
	else if (input == 3)
		return true;
	else if (input == 5)
		return true;
	else if (input == 7)
		return true;
	else
		return false;
}

int main()
{
	std::cout << "Enter a digit: ";
	int x{};
	std::cin >> x;

	if (isPrime(x))
		std::cout << "The digit is a prime number";
	else
		std::cout << "The digit is not a prime number";

	return 0;
}