#include <iostream>

int main()
{
	//showing type conversion between char and int using static_cast
	char ch{ 'a' };
	std::cout << ch << '\n';						// a
	std::cout << static_cast<int>(ch) << '\n';		// 97
	std::cout << ch << '\n';						// a
													// static_cast doesn't affect the value store in the variable

	//show a char and ASCII code simultaneously
	std::cout << std::endl;
	std::cout << "Input a keyboard character: ";								// a

	char ch2{};
	std::cin >> ch2;
	std::cout << ch2 << " has ASCII code " << static_cast<int>(ch2) << '\n';	// a has ASCII code 97

	//showing several chars and ASCII code 
	// ch3 can store only one character
	std::cout << std::endl;
	std::cout << "Input a keyboard character: ";								// abcd

	char ch3{};
	std::cin >> ch3;
	std::cout << ch3 << " has ASCII code " << static_cast<int>(ch3) << '\n';	// a has ASCII code 97

	std::cin >> ch3;
	std::cout << ch3 << " has ASCII code " << static_cast<int>(ch3) << '\n';	// b has ASCII code 98

	std::cin >> ch3;
	std::cout << ch3 << " has ASCII code " << static_cast<int>(ch3) << '\n';	// c has ASCII code 99

	//the rest of it won't be shown

	return 0;
}