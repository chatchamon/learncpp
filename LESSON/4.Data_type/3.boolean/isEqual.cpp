#include <iostream>

bool isEqual(int a, int b)
{
	return (a == b);
}

int main()
{
	std::cout << "Enter an integer: ";
	int x{};
	std::cin >> x;

	std::cout << "Enter aother integer: ";
	int y{};
	std::cin >> y;

	std::cout << std::boolalpha;

	if (isEqual(x, y))
		std::cout << x << " is equal to " << y << '\n';
	else
		std::cout << x << " is not equal to " << y << '\n';

	//std::cout << "is " << x << " equal " << y << " ? ";
	//std::cout << isEqual(x, y) << '\n';

	return 0;

}