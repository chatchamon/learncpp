#include <iostream>

int main()
{
	std::cout << std::boolalpha;

	bool b1 = 4;
	std::cout << b1 << '\n';

	bool b2 = 0;
	std::cout << b2 << '\n';

	return 0;
}