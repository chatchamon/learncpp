#include <iostream>

int main()
{
	bool b1{ true };
	std::cout << b1 << '\n';

	std::cout << std::boolalpha;
	std::cout << b1 << '\n';

	return 0;
}