#include <iostream>

double userInput()
{
	std::cout << "Enter a double value: ";
	double input{};
	std::cin >> input;
	return input;
}

char receiveSymbol()
{
	std::cout << "Enter one of the following: +, -, *, or /: ";
	char operation{};
	std::cin >> operation;
	return operation;
}

void printMath(double a, double b, char symbol)
{
	if (symbol == '+')
		std::cout << a << " + " << b << " is " << a + b << '\n';
	else if (symbol == '-')
		std::cout << a << " - " << b << " is " << a - b << '\n';
	else if (symbol == '*')
		std::cout << a << " * " << b << " is " << a * b << '\n';
	else if (symbol == '/')
		std::cout << a << " / " << b << " is " << a / b << '\n';
	else
		std::cout << std::endl;

}

int main()
{

	double x{ userInput() };
	double y{ userInput() };
	char arit{ receiveSymbol() };
	printMath(x, y, arit);

	return 0;
}