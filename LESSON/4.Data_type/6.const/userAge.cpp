#include <iostream>

int main()
{
	std::cout << "Enter your age: ";
	int age{0};
	std::cin >> age;

	const int userAge(age);									//user age is constant, cannot change after this statement

	std::cout << "User's age is: " << userAge << '\n';


	return 0;
}