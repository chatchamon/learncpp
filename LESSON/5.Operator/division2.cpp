#include <iostream>

int main()
{
	std::cout << "Enter a divisor: ";
	int x{};													//if you enter 0, the program will crash
	std::cin >> x;												//cannot divide by 0

	std::cout << "12/ " << x << " = " << 12 / x << '\n';

	return 0;
}