#include <iostream>

int userInput()
{
	std::cout << "Enter an integer: ";
	int input{};
	std::cin >> input;
	return input;
}

bool isEven(int x)
{
	return(x % 2 == 0);
}

int main()
{
	int x{ userInput() };

	if (isEven(x) == true)
		std::cout << x << " is even\n";
	else
		std::cout << x << " is odd\n";
	
	return 0;
}