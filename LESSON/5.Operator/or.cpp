#include <iostream>

int main()
{
	std::cout << "Enter an integer: ";
	int x{};
	std::cin >> x;

	if (x == 0 || x == 1 || x == 2 || x == 3)
		std::cout << "You either picked 0,1,2,3\n";
	else
		std::cout << "You didn't pick 0,1,2,3\n";

	return 0;
}