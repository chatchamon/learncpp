#include <iostream>

int main()
{
	int x{ 5 };									// 5
	int y = ++x;								// 6

	std::cout << x << ' ' << y << '\n';			// 6 6

		
	int a{ 5 };									// 5
	int b = a++;								// 6

	std::cout << a << ' ' << b << '\n';			// 6 5
	
	return 0;
}