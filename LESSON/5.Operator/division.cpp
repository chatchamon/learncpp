#include <iostream>

int main()
{
	int x{ 7 };
	int y{ 4 };

	//to show that int/int may get round up answer
	//but when cooperate with some double, float you will a more exact answer

	std::cout << "int / int = " << x / y << '\n';
	std::cout << "double / int = " << static_cast<double> ( x ) / y << '\n';
	std::cout << "int / double = " << x / static_cast<double>(y) << '\n';
	std::cout << "double / double = " << static_cast<double>(x) / static_cast<double>(y) << '\n';
	return 0;
}