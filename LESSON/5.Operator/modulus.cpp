#include <iostream>

//should acknowlefe that incase input is negative
// 6%-4 = 2, -6%4 = -2
//symbol depens on the first operand

int getUserInput()
{
	std::cout << "Enter an integer: ";
	int input{};
	std::cin >> input;
	return input;
}

void printModulus(int a, int b)
{
	if ((a % b) == 0)
		std::cout << a << " is evenly divisible by " << b << '\n';
	else
		std::cout << a << " is not evenly divisible by " << b << '\n';
}

int main()
{
	int x{ getUserInput() };
	int y{ getUserInput() };

	std::cout << "The remainder is: " << x % y << '\n';

	printModulus(x, y);

	return 0;
}