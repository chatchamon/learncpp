#include <iostream>			//input-output
#include <cmath>			//pow(double,double)
#include <cstdint>			//for std::int_fast64_t

//note: exp must be non-negative
std::int_fast64_t powint(int base, int exp)			//function for integer power
{
	std::int_fast64_t result{ 1 };
	while (exp)
	{
		if (exp & 1)
			result *= base;
		exp >>= 1;
		base *= base;
	}
	return result;
}

int main()
{
	double x{ std::pow(3.0, 4.0) };				//pow(double, double) might not give precise answer
	std::cout << x << '\n';

	std::cout << powint(7, 12);					//In the vast majority of cases, integer exponentiation will overflow the integral type. 
												//This is likely why such a function wasn�t included in the standard library in the first place.

	return 0;
}