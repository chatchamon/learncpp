#include <iostream>

int main()
{
	std::cout << "Enter an integer: ";
	int x{};
	std::cin >> x;

	if (x >= 0)
	{
		std::cout << "It is a positive number\n";
		std::cout << "Double this = " << x * 2 << '\n';
	}
	else
	{
		std::cout << "It is a negative number\n";
		std::cout << "The positive of this number is " << -x << '\n';
	}

	return 0;
}