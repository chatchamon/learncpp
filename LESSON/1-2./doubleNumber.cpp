// doubleNumber.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

int main()
{
	int x{};
	std::cout << "Enter an integer: ";
	std::cin >> x;
	std::cout << "Double " << x << " is " << x * 2 << '\n';
	std::cout << "Triple " << x << " is " << x * 3 << '\n';
	//return 0;

	std::cout << std::endl;
	std::cout << "Enter an interger: ";
	int y{};
	std::cin >> y;

	std::cout << "Enter another interger: ";
	int z{};
	std::cin >> z;

	std::cout << y << " + " << z << " is " << y + z << ".";
	std::cout << std::endl;
	std::cout << y << " - " << z << " is " << y - z << ".";
	std::cout << std::endl;

	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
