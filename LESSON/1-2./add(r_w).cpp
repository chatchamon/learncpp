#include <iostream>
#include "io.h"

int readNumber();
void writeAnswer(int adding);

int main()
{
    //first way: doing adding directly
    //writeAnswer(readNumber() + readNumber());           //not recommend as hard to check how many input adding together

    //second way: assign value to each input
    int x{ readNumber() };
    int y{ readNumber() };
    writeAnswer(x + y);
    return 0;
}
