#include <iostream>

int main()
{
	std::cout << "Enter an integer: ";
	int x{};
	std::cin >> x;

	if (x > 10 && x < 20)
		std::cout << "Your value is between 10 and 20\n";
	else
		std::cout << "Your value is not between 10 and 20\n";

	return 0;
}