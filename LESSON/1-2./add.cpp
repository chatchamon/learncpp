#include <iostream>

int enter1Int()
{
	int a{};
	std::cout << "Enter an int: ";
	std::cin >> a;
	return a;
}

int addInt( int x, int y )
{
	std::cout << y << " + " << x << " is " << x + y;
	std::cout << std::endl;											//start new line
	return 0;														//actually no need to return x+y as we would like to just print
	//return x + y;
}

int main()
{
	addInt( enter1Int(), enter1Int() );
	return 0;
}