#include <functional>	// for std::reference::wrapper
#include <iostream>
#include <vector>
#include <string>

int main()
{
	std::string tom{ "Tom" };
	std::string berta{ "Berta" };

	std::vector<std::reference_wrapper<std::string>>names{ tom,berta };

	std::string jim{ "Jim" };
	names.push_back(jim);

	for (auto name : names)
		name.get() += " Beam";

	std::cout << jim << '\n';

	return 0;
}