#include <functional>	//std::reference_wrapper
#include <string>
#include <vector>
#include <iostream>

class Teacher
{
private:
	std::string m_name{};

public:
	Teacher(const std::string& name)
		:m_name{ name }
	{
	}

	const std::string& getName() const { return m_name; }
};

class Department
{
private:
	std::vector<std::reference_wrapper<const Teacher>> m_teachers{};

public:
	void add(const Teacher& teacher)
	{
		m_teachers.push_back(teacher);
	}

	friend std::ostream& operator<<(std::ostream& out, const Department& department)
	{
		out << "Department: ";
		for (auto element : department.m_teachers)
			out << element.get().getName() << ' ';
		out << '\n';
		return out;
	}
};

int main()
{
	Teacher t1{ "Bob" };
	Teacher t2{ "Frank" };
	Teacher t3{ "Beth" };
	{
		Department department{};
		department.add(t1); 
		department.add(t2);
		department.add(t3);
		std::cout << department;
	}
	std::cout << t1.getName() << " still existed\n";
	std::cout << t2.getName() << " still existed\n";
	std::cout << t3.getName() << " still existed\n";
}