#ifndef POINT2D_H
#define POINT2D_H

class Point2D
{
private:
	int m_x;
	int m_y;

public:
	//defualt constructor
	Point2D()
		:m_x{ 0 }, m_y{ 0 }
	{
	}

	// a specific constructor
	Point2D(int x, int y)
		:m_x{ x }, m_y{ y }
	{
	}

	//an overload output operator
	friend std::ostream& operator<<(std::ostream& out, const Point2D& p)
	{
		out << "(" << p.m_x << "," << p.m_y << ")";
		return out;
	}

	void setPoint(int x, int y)
	{
		m_x = x;
		m_y = y;
	}
};

#endif