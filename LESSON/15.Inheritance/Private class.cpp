#include <iostream>

class Base
{
public:
	int m_public;
protected:
	int m_protected;
private:
	int m_private;
};

class Pri : private Base
{
public:
	Pri()
	{
		m_public = 1;
		m_protected = 2;
		//m_protected = 3;
	}
};

int main()
{
	Base base;
	base.m_public = 1;
	//base.m_protected = 2;
	//base.m_private = 3;

	Pri pri;
	//pri.public = 1;
	//pri.protected = 2;
	//pri.private = 3;

}