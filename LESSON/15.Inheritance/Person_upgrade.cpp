#include <iostream>
#include <string>

class Person
{
private:
	std::string m_name;
	int m_age;

public:
	Person(const std::string& name, int age)
		:m_name{ name }, m_age{ age }
	{
	}

	const std::string& getName() const { return m_name; }
	int getAge() const { return m_age; }
};

class BaseballPlayer : public Person
{
private:
	double m_bettingAverage;
	int m_homeRuns;

public:
	BaseballPlayer(const std::string& name = "", int age = 0,
		double bettingAverage = 0.0, int homeRuns = 0)
		:Person{ name, age },
		m_bettingAverage{ bettingAverage }, m_homeRuns{ homeRuns }
	{
	}

	double getBettingAvg() const { return m_bettingAverage; }
	int getHomeRun() const { return m_homeRuns; }
};

int main()
{
	BaseballPlayer pedro{ "Pedro", 31, 0.342, 42 };
	std::cout << pedro.getName() << " " << pedro.getAge() << " " << pedro.getHomeRun() << '\n';

	return 0;
}