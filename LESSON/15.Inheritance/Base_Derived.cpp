#include <iostream>

class Base
{
public:
	int m_id;

	Base(int id=0)
		:m_id{ id }
	{
		//std::cout << "Based\n";
	}

	int getID() const { return m_id; }
};

class Derived : public Base
{
public:
	double m_cost;

	Derived(double cost = 0.0, int id=0)
		:Base{id},
			m_cost{ cost }
	{
		//std::cout << "Derived\n";
	}

	double getCost() const { return m_cost; }
};

int main()
{
//test1
	//std::cout << "Instantiating Base\n";
	//Base cBase;

	//std::cout << "Instantiating Derived\n";
	//Derived cDerived;

//test2
	//Base base{ 5 };
	//Derived derived{ 1.3 };

//test3
	Derived derived{ 1.3,5 };
	std::cout << "Id: " << derived.getID() << " ";
	std::cout << "Cost: " << derived.getCost() << '\n';
	return 0;
}
