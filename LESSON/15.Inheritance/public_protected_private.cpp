#include <iostream>

class Base
{
public:
	int m_public; //can be accessed by anybody
protected:
	int m_protected; // can be accessed by Base members, friends, and derived classes
private:
	int m_private; // can only be accessed by Base members and friends (but not derived classes)
};

class Derived : public Base
{
	Derived()
	{
		m_public = 1;
		m_protected = 2;
		//m_private = 3;	// not allowed
	}
};

int main()
{
	Base base;
	base.m_public = 1;
	//base.m_protected = 2;
	//base.m_privated = 3;
}