#include <iostream>

int main()
{
	int count = 0;
	while (count != 11)
	{
		std::cout << count << ' ';
		++count;
	}
	std::cout << "done!" << '\n';
	return 0;
}