#include <iostream>

int main()
{
	char c{ 'a' };
	int n = static_cast<int>(c);
	std::cout << c << ' ' << n << '\n';
	while (n < 122)
	{
		++n;
		char d = static_cast<char>(n);
		std::cout << d << ' ' << n << '\n';
	}
	return 0;
}