#include <iostream>

int main()
{
	int i = 1;
	while (i < 10)
	{
		std::cout << "0" << i << ' ';
		++i;
	}
	while (i <= 50)
	{
		std::cout << i << ' ';
		if (i % 10 == 0 && i >= 10)
		{
			std::cout << '\n';
		}
		++i;
	}		
	return 0;
}