#include <iostream>

//actually you may not need a and line, but i'm confused while wrote this code
//         1
//       2 1
//     3 2 1			<- expected result
//   4 3 2 1
// 5 4 3 2 1

int main()
{
	int line = 1;			// we need five line (line#1, line#2, line#3, line#4, line#5)
	int a = 5;				// amount of member in each line (there are five members in each line)
							// ex. x x x x 1
	int outer = 5;			// 5 lines
	int inner = 1;			// written number in each line (ex. x x 2 1) 
								//-> 2, 1 are the written numbers
	
	while (outer != 0)
	{
		int x = a - line;	// amount of x we need for each line
		inner = line;		// upgrade written number to equal to the line# 
								// line#2 ->written 2, 1
		while (x != 0)		//until all x are written, in this case is blank space
		{
			std::cout << "  ";		//substitute x with blank
			x--;
		}
		while(inner != 0)	//until all written number are written
		{
			std::cout << inner << ' ';
			--inner;
		}
		std::cout << '\n';	// start a new line
		++line;				// starting new line#
		--outer;			// reduce the round of running
	}
	return 0;
}