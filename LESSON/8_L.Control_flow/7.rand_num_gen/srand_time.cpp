#include <iostream>
#include <cstdlib> // for std::rand() and std::srand()
#include <ctime> // for std::time()

int main()
{
	std::srand(static_cast<unsigned int>(std::time(nullptr))); // set intial seed value to system clock

	for (int i{ 1 }; i <= 100; ++i)
	{
		std::cout << std::rand() << '\t';

		if (i % 5 == 0)
			std::cout << '\n';
	}
	return 0;
}
