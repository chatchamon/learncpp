#include "random.hpp"
#include <iostream>

//to include external library
//1. copy the file to the project's folder
//2. add > existing item > and include the file

using Random = effolkronium::random_static;

int main()
{
	std::cout << Random::get(1, 6) << '\n';
	std::cout << Random::get(1, 10) << '\n';
	std::cout << Random::get(1, 20) << '\n';

	return 0;
}