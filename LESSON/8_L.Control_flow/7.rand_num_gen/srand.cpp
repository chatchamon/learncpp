#include <iostream>
#include <cstdlib> // for std::rand() and std::srand()

int main()
{
	std::srand(5323); //set initial seed value to 5323

	for (int i{ 1 }; i <= 100; ++i)
	{
		std::cout << std::rand() << '\t';

		if (i % 5 == 0)
			std::cout << '\n';
	}

	return 0;
}