#include <iostream>

enum class ErrorCode
{
	SUCCESS = 0,
	NEGATIVE_NUMBER = -1,
};

int getUserInput()
{
	std::cout << "Enter a positive number: ";
	int number{};
	std::cin >> number;
	return number;
}

ErrorCode checkInput(int number)
{
	if (number >= 0)
		return ErrorCode::SUCCESS;
	else
		return ErrorCode::NEGATIVE_NUMBER;
}

std::string printResult(ErrorCode errorcode)
{
	switch(errorcode)
	{
	case ErrorCode::SUCCESS: 
		std::cout << "Success!\n";
		break;
	case ErrorCode::NEGATIVE_NUMBER:
		std::cout << "This is a negative number!\n";
		break;
	}
	return "unknown";
}

int main()
{
	int x{ getUserInput() };
	printResult(checkInput(x));

	return 0;
}