#include <iostream>

int main()
{
//first example
	switch (2)
	{
	case 1:
		std::cout << 1 << '\n';
	case 2:
		std::cout << 2 << '\n';
	case 3:
		std::cout << 3 << '\n';
	default:
		std::cout << 5 << '\n';
	}

//second example
	switch (1)
	{
	case 1:
	{ // note addition of block here
		int x = 4; // okay, variables can be initialized inside a block inside a case
		std::cout << x;
		break;
	}
	default:
		std::cout << "default case" << std::endl;
		break;
	}

	return 0;
}
