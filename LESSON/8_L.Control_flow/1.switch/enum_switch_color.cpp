#include <iostream>
#include <string>

enum class Color
{
	RED,
	BLUE,
	GREEN,
};

void printColor(Color color)
{
	switch (color)
	{
	case Color::RED: std::cout << "Red"; break;
	case Color::BLUE: std::cout << "Blue"; break;
	case Color::GREEN: std::cout << "Green"; break;
	}
}

int main()
{
	Color color = Color::RED;
	printColor(color);

	return 0;
}