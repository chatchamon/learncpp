#include <iostream>
#include <string>

enum class Animal
{
	CHICKEN,
	GOAT,
	CAT,
	DOG,
	OSTRICH,
};

std::string getAnimalName(Animal animal)
{
	switch (animal)
	{
	case Animal::CHICKEN:
		return "chicken";
		break;
	case Animal::GOAT:
		return "goat";
		break;
	case Animal::CAT:
		return "cat";
		break;
	case Animal::DOG:
		return "dog";
		break;
	case Animal::OSTRICH:
		return "ostrich";
		break;
	default:
		return "There is not in the list\n";
		break;
	}
	return "unknown";
}

void printNumberofLeg(Animal animal)
{
	std::cout << "A " << getAnimalName(animal) << " has ";
	switch (animal)
	{
	case Animal::CHICKEN:
	case Animal::OSTRICH:
		std::cout << "2 legs\n";
		break;

	case Animal::GOAT:
	case Animal::CAT:
	case Animal::DOG:
		std::cout << "4 legs\n";
		break;
	default:
		std::cout << "There is not in the list\n";
		break;
	}
}

int main()
{
	printNumberofLeg(Animal::CHICKEN);
	printNumberofLeg(Animal::GOAT);
	printNumberofLeg(Animal::CAT);
	printNumberofLeg(Animal::DOG);
	printNumberofLeg(Animal::OSTRICH);
	//printNumberofLeg(Animal::HUMAN);
	return 0;
}