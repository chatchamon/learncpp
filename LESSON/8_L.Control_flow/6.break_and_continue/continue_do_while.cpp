#include <iostream>

int main()
{
	int count{ 0 };
	do 
	{
		if (count == 5)
			continue;
		std::cout << count << ' ';

	} while (++count < 10);

	return 0;
}