#include <iostream>

int breakOrReturn()
{
	while (true)
	{
		std::cout << "Enter 'b' to break or 'r' to return: ";
		char input{};
		std::cin >> input;

		if (input == 'b')
			break;
		else if (input == 'r')
			return 1;
	}
	std::cout << "We are breaking\n";
	return 0;
}

int main()
{
	int a{ breakOrReturn() };
	std::cout << "Function breakOrReturn returned " << a << '\n';

	return 0;
}