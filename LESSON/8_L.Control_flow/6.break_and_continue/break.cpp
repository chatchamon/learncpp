#include <iostream>

int main()
{
	int sum{ 0 };

	for (int i{ 0 }; i <= 10; i++)
	{
		std::cout << "Enter a number, or 0 for exit: ";
		int x{};
		std::cin >> x;

		sum += x;

		if (x == 0)
			break;
	}

	std::cout << "Total summing is: " << sum << '\n';

	return 0;
}