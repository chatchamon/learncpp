#include <iostream>

int sumTo(int input)
{
	int sum = 0;
	for (; input != 0; --input)
		sum += input;

	return sum;
}

int main()
{
	for (int i{ 0 }; i <= 20; i+=2)
		std::cout << i << ' ';

	std::cout << std::endl;
	std::cout << sumTo(7);

		return 0;
}