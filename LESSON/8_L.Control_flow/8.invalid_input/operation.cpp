#include <iostream>

int getUserNum()
{
	while (true)
	{
		std::cout << "Enter a number: ";		//int range +-(2147483648)
		int input{};
		std::cin >> input;

		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(32767, '\n');
			std::cout << "Enter again!\n";
		}
		else
		{
			std::cin.ignore(32767, '\n');
			return input;
		}
	}
}

char getOperation()
{
	while (true)
	{
		std::cout << "Enter an operation (+,-,*, or /): ";
		char op{};
		std::cin >> op;

		if (op == '+' || op == '-' || op == '*' || op == '/')
			return op;
		else
			std::cout << "Enter again!\n";
	}
}

int calculate(int x, int y, char a)
{
	switch (a)
	{
	case('+'): return x + y; break;
	case('-'): return x - y; break;
	case('*'): return x * y; break;
	case('/'): return x / y; break;
	}
}

int main()
{
	int x{ getUserNum() };
	char a{ getOperation() };
	int y{ getUserNum() };

	
	std::cout << x << ' ' << a << ' ' << y << " = " << calculate(x, y, a) << '\n';

	return 0;
}