#include <iostream>
#include <random> // for std::mt19937
#include <ctime> // for std::time

namespace MyRandom
{
	std::mt19937 mersenne{ static_cast<std::mt19937::result_type>(std::time(nullptr)) };
}

int getRandomNumber(int min, int max)
{
	std::uniform_int_distribution<> die{ min, max };
	return die(MyRandom::mersenne);
}


void getInputAndCompare(int number)
{
	std::cout << "Let's play a game. I'm thinking of a number. You have 7 tries to guess what it is.\n";
	for (int i{ 1 }; i != 8; i++)
	{
		std::cout << "Guess #" << i << " :";
		int input{};
		std::cin >> input;
		std::cin.ignore(32767, '\n');

		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(32767, '\n');
			std::cout << "Oops, that input is invalid.  Please try again.\n";
			--i;
		}
		else if (input < number)
			std::cout << "Your guess is too low\n";
		else if (input > number)
			std::cout << "Your guess is too high\n";
		else if (input == number)
		{
			std::cout << "Correct! You win!\n";
			break;
		}
		if (input != number && i == 7)
			std::cout << "Sorry, you lose. The correct number was " << number << '\n';
	}
}

bool playAgain()
{
	while (true)
	{
		std::cout << "Would you like to play agian: ";
		char insert{};
		std::cin >> insert;

		if (insert == 'y')
		{
			int number2{ getRandomNumber(1,6) };
			getInputAndCompare(number2);
		}
		else if (insert == 'n')
		{
			std::cout << "Thank you for playing\n";
			return false;
		}
		else
		{
			std::cin.clear();
			std::cin.ignore(32767, '\n');
		}
	}
}

int main()
{
	int number = getRandomNumber(1, 6);
std::cerr << number << '\n';
	
	getInputAndCompare(number);
	playAgain();

	return 0;
}