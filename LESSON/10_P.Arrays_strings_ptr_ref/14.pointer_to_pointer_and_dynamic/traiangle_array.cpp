#include <iostream>

int main()
{
	int** array = new int* [10];
	for (int count{ 0 }; count < 10; ++count)
		array[count] = new int[count + 1];

	// probably not correct
	for (int index{ 0 }; index < 10; ++index)
		for (int innerIndex{ 0 }; innerIndex < 11; ++innerIndex)
			std::cout << array[index][innerIndex] << '\t';
	std::cout << '\n';

	//delete the array
	for (int count{ 0 }; count < 10; ++count)
		delete[] array[count];
	delete[] array;
		
	

}