

int main()
{
	int** array = new int* [10];		// allocate an array of 10 int pointer

	int array[10][5];					// int - int array
	
// two dimensional pointer array
	int** array = new int*[10];
	for (int count{ 0 }; count < 10; ++count)	// int - pointer array
		array[count] = new int[5];

	return 0;
}