#include <iostream>

int main()
{
	int array[]{ 9,7,5,3,1 };

	std::cout << "Element 0 is at address: " << &array[0] << '\n';		// 004FF958
	std::cout << "Element 1 is at address: " << &array[1] << '\n';		// 004FF95C  -*
	std::cout << "Element 2 is at address: " << &array[2] << '\n';		// 004FF960
	std::cout << "Element 3 is at address: " << &array[3] << '\n';		// 004FF964
	std::cout << std::endl;

	std::cout << &array[1] << '\n';			// 004FF95C -*
	std::cout << array + 1 << '\n';			// 004FF95C -*

	std::cout << array[1] << '\n';			// 7
	std::cout << *(array + 1) << '\n';		// 7

	return 0;
}