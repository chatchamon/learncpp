#include <iostream>
#include <iterator>		// for std::size
						// for std::begin, std::end
#include <algorithm>

bool isVowel(char ch)
{
	switch (ch)
	{
	case 'A': case 'a': case 'E': case 'e': case 'I': case 'i': case 'O': case 'o': case 'U': case 'u':
		return true;
	default:
		return false;
	}
}


int main()
{
	char name[]{ "Chatchamon" };
	int length = static_cast<int>(std::size(name));
	
	//straightforward method
	/*int numVowels{ 0 };
	for (char* ptr{ name }; ptr < (name + length); ++ptr)
		if (isVowel(*ptr))
			++numVowels;
	*/

	//use std::count_if
	auto numVowels{ std::count_if(std::begin(name), std::end(name), isVowel) };

	std::cout << name << " has " << numVowels << " vowels.\n";


	return 0;



}