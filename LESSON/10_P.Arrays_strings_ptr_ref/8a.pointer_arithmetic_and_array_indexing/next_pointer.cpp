﻿#include <iostream>

int main()
{
//first example
	int value_int{ 7 };
	int* ptr{ &value_int };

	// ห่างกันทีละ 4 ตำเเหน่ง ตาม memory ของ int (1 int = 4 byte)
	std::cout << ptr << '\n';		
	std::cout << ptr + 1 << '\n';		
	std::cout << ptr + 2 << '\n';
	std::cout << ptr + 3 << '\n' << '\n';


// second example
	short value_sh{ 7 };
	short* ptr_2{ &value_sh };

	// ห่างกันทีละ 2 ตำเเหน่ง ตาม memory ของ short (1 short = 2 byte)
	std::cout << ptr_2 << '\n';
	std::cout << ptr_2	+ 1 << '\n';
	std::cout << ptr_2  + 2 << '\n';
	std::cout << ptr_2  + 3 << '\n';



	return 0;
}