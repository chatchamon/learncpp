#include <iostream>
#include <cstring>
#include <iterator>		// for std::size

int main()
{
	std::cout << "Enter a string: ";
	char buffer[255];
	std::cin.getline(buffer, std::size(buffer));

	int spaceFound{ 0 };
	int bufferLength = static_cast<int>(std::strlen(buffer));
	
	for (int index{ 0 }; index < bufferLength; ++index)
	{
		if (buffer[index] == ' ')
			++spaceFound;
	}

	std::cout << "You typed " << spaceFound << " spaces!\n";

	return 0;

}
