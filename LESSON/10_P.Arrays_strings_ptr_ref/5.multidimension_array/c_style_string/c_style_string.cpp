#include <iostream>
#include <iterator>		// for std::size
	
int main()
{

//first example
	//create a char array
	char myString[]{ "string" };		// length = 7
										// s	t	r	i	n	g	null_termiator

	const int length = static_cast<int>(std::size(myString));

	std::cout << myString << " has " << length << " characters.\n";

	for (int i{ 0 }; i < length; ++i)
		std::cout << myString[i] << '\t';
	std::cout << std::endl;

	for (int j{ 0 }; j < length; ++j)
		std::cout << static_cast<int>(myString[j]) << '\t';
	std::cout << std::endl;

	std::cout << std::endl;

//second example
	char name[20]{ "Alex" };			//although we declare an array which holds up to 20 character
										//but the program will hit null_terminator, and print only "Alex"
	std::cout << "My name is: " << name << '\n';

	std::cout << std::endl;

//third example
	char name_insert[255];
	std::cout << "Enter your name: ";
	std::cin.getline(name_insert, std::size(name));
	std::cout << "You entered: " << name_insert << '\n';

	return 0;


}