#include <iostream>
#include <cstring>					// for std::strcpy
#define __STDC_WANT_LIB_EXT1__ 1		

int main()
{
	char source[]{ "Copy this!" };
	char dest[50];
	
	strcpy_s(dest, source);
	std::cout << dest << '\n';

	return 0;

}