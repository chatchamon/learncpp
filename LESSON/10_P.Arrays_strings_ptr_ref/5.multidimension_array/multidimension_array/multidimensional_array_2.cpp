#include <iostream>
#include <iterator>		// for std::size

int main()
{
	int array2[4][4] = { {1,2,3,4}, {5,6,7,8}, {9,10,11,12} };

	// this one is not allowed
	//int array2[][] = {{1,2,3,4}, {5,6,7,8}, {9,10,11,12}};

	int length2 = static_cast<int>(std::size(array2));

	int array3[3][1][2];
	for (int i{ 0 }; i < 3; ++i)
	{
		for (int j{ 0 }; j < 1; ++j)
		{
			for (int k{ 0 }; k < 2; ++k)
			{
				std::cout << array3[i][j][k] << '\t';
			}
			std::cout << '\n';
		}
		std::cout << '\n';
	}

	std::cout << array3[3][1][2];
		
	return 0;


}