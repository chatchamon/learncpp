// produce a multiplication table from 1x1 to 12x12

#include <iostream>

int main()
{
	constexpr int numRow{ 13 };
	constexpr int numCol{ 13 };

	// declare an array
	int product[numRow][numCol]{};

	// calculate
	for (int row{ 1 }; row < numRow; ++row)
		for (int col{ 1 }; col < numCol; ++col)
			product[row][col] = row * col;

	// print the table
	for (int row{ 1 }; row < numRow; ++row)
	{
		for (int col{ 1 }; col < numCol; ++col)
		{
			std::cout << product[row][col] << '\t';
		}
		std::cout << '\n';
	}
		
	return 0;
}