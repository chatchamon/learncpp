#include <iostream>
#include <iterator>		// for std::size

int main()
{
	int array[3][5]
	{
	{1,2,3,4,5},
	{6,7,8,9,10},
	{11,12,13,14,15}
	};

	int length = static_cast<int>(std::size(array));

	for (int i{ 0 }; i < 3; ++i)					// by each row
	{
		for (int j{ 0 }; j < 5; ++j)				// by each element in the row
		{
			std::cout << array[i][j] << '\t';
		}
		std::cout << std::endl;
	}
	return 0;

}