#include <iostream>
#include <string>
#include <iterator>		// for std::sizeof

int main()
{
	std::cout << "Enter a positive integer: ";
	int length{};
	std::cin >> length;

	int* array{ new int [length] {} };
	std::cout << "I just allocated an array of integers of length: " << length << '\n';

	array[0] = 5;

	for (int index{ 0 }; index < length; ++index)
		std::cout << "array" << "[" << index << "]" << " is " << array[index] << '\n';
	std::cout << std::endl;

	delete[] array;

// dynamic array and fixed array
	int fixedArray[5] = { 9,7,5,3,1 };
	int* array_1{ new int[5]{9,7,5,3,1} };

	std::cout << fixedArray << ' ' << array_1 << '\n';
	std::cout << std::endl;

// string initialized dynamic array
	std::string *st = new std::string[20]{ "Hello World!" };

	for (int i{ 0 }; i < sizeof(st); ++i)
		std::cout << "st" << "[" << i << "]" << " is " << st[i] << '\n';
	std::cout << std::endl;
	
	return 0;
}