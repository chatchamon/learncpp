#include <iostream>

int main()
{
	int* ptr{ new int{} };
	int* otherPtr{ ptr };

	delete ptr;
	ptr = nullptr;

// in case operator fail, and 'new' is not working
	int* value = new(std::nothrow) int{};
	if (!value)
		std::cout << "Could not allocate memory";

// null pointer
	if (!ptr)
		ptr = new int;
	//if (ptr)
	delete ptr;

	return 0;
}