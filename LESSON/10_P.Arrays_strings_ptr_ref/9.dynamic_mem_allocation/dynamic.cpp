#include <iostream>

int main()
{
// initialize a dynamic variable
	int* ptr{ new int(5) };
	int* ptr2{ new int{6} };

	std::cout << ptr << ' ' << ptr2 << '\n';

// delete single variable
	delete ptr2;
	ptr2 = 0;
	std::cout << ptr2 << '\n';

	return 0;

}