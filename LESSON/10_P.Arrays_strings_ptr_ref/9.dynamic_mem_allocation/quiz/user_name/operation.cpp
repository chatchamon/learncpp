#include "operation.h"
#include <iostream>
#include <algorithm>	// for std::swap
#include <limits>		// for std::numeric_limits
#include <string>

int getNumName()
{
	std::cout << "How many names you would like to enter?: ";
	int num{};
	std::cin >> num;

	return num;
}

std::string* getName(int num)
{
	std::string* name{ new std::string[num]{} };
	for (int index{ 0 }; index < num; ++index)
	{
		std::cout << "Enter name #" << index + 1 << " : ";
		std::cin >> name[index];
	}
	return name;
}

std::string* sortName(std::string* name, int num)
{
	for (int startIndex{ 0 }; startIndex < num - 1; ++startIndex)
	{
		int smallestIndex = startIndex;
		for (int currentIndex{ startIndex + 1 }; currentIndex < num; ++currentIndex)
		{
			if (name[currentIndex] < name[smallestIndex])
				smallestIndex = currentIndex;
		}
		std::swap(name[startIndex], name[smallestIndex]);
	}
	return name;
}

void printSortedName(std::string* name, int num)
{
	std::cout << "\nHere is your sorted list:\n";
	for (int index{ 0 }; index < num; ++index)
		std::cout << "Name #" << index + 1 << ' ' << name[index] << '\n';
}
