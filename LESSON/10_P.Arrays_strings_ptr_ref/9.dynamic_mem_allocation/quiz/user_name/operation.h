#include <string>

#ifndef OPERATION_H
#define OPERATION_H

int getNumName();
std::string* getName(int num);
std::string* sortName(std::string* name, int num);
void printSortedName(std::string* name, int num);



#endif