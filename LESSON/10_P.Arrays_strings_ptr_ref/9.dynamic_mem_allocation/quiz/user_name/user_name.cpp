#include <iostream>
#include <algorithm>	// for std::swap
#include <limits>		// for std::numeric_limits
#include <string>
#include "operation.h"

int main()
{
	int num = getNumName();
	std::string* name{ getName(num) };

	printSortedName(sortName(name, num), num);
	
	delete[] name;

	return 0;

}