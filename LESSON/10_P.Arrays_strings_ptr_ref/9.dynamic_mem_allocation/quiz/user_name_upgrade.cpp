#include <iostream>
#include <algorithm>	// for std::sort
#include <limits>		// for std::numeric_limits
#include <string>

int getNameCount()
{
	std::cout << "How many names would you like to enter? ";
	int length{};
	std::cin >> length;

	return length;
}

void getNames(std::string* names, int length)
{
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	for (int i{ 0 }; i < length; ++i)
	{
		std::cout << "Enter name #" << i + 1 << ": ";
		std::getline(std::cin, names[i]);
	}
}

void printNames(std::string* names, int length)
{
	std::cout << "\nHere is your sorted list: \n";

	for (int i{ 0 }; i < length; ++i)
		std::cout << "Name #" << i + 1 << ": " << names[i] << '\n';
}

int main()
{
	int length(getNameCount());

	std::string* names{ new std::string[static_cast<std::size_t>(length)] };
	getNames(names, length);

	std::sort(names, names + length);
	printNames(names, length);

	delete[] names;

	return 0;

}