#include <iostream>

int main()
{
	double array[3];
	array[0] = 2.0;
	array[1] = 5.2;
	array[2] = 7.8;

	std::cout << "sum all elements in the array: " << array[0] + array[1] + array[2] << '\n';

	return 0;
}