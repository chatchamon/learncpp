#include <iostream>

int main()
{
	int array[5];
	
	array[1] = 7;

	enum Animals
	{
		ANIMAL_CAT = 2,
	};

	array[ANIMAL_CAT] = 4;

	short index = 3;
	array[index] = 7;

	array[2 + 2] = 8;

	std::cout << "array[0] = " << array[0] << '\n';
	std::cout << "array[1] = " << array[1] << '\n';
	std::cout << "array[2] = " << array[2] << '\n';
	std::cout << "array[3] = " << array[3] << '\n';
	std::cout << "array[4] = " << array[4] << '\n';


	return 0;
}