//this file cannot compile, only for showing

int main()
{
//initialize array range

	// using a literal constant
	int array[5];	

	//using a symbolic constant
	const int arraylength = 5;
	int array[arraylength];

	//using an enumerator
	enum ArrayElements
	{
		MAX_ARRAY_LENGTH = 5,
	};
	int array[MAX_ARRAY_LENGTH];

	return 0;
}