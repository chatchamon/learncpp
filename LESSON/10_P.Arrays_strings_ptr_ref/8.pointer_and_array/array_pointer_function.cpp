#include <iostream>

//void printSize(int array[])					// case 1
void printSize( int *array)						// case 2	... both case run the same (give the same result)
{
	std::cout << sizeof(array) << '\n';
}

int main()
{
	int array[]{ 1,1,2,3,5,8,13,21 };
	std::cout << sizeof(array) << '\n';

	printSize(array);

	return 0;
}