#include <iostream>

int main()
{
// array decay
	int array[]{ 9,7,5,3,1 };
	// print address of the array's first element
	std::cout << "Element 0 has address: " << &array[0] << '\n';							// 001F2D

	// print the value of the pointer the array decays to
	std::cout << "The array decays to pointer holding address: " << array << '\n' << '\n';	// 001F2D

//deref the array
	int array2[5] = { 9,7,5,3,1 };

	std::cout << *array << '\n';			// 9

	char name[]{ "Jason" };
	std::cout << *name << '\n' << '\n';		// J

// array and pointer
	int array3[5] = { 9,7,5,3,1 };
	std::cout << *array << '\n';			// 9

	int* ptr3{ array };
	std::cout << *ptr3 << '\n' << '\n';		// 9

// difference btw pointers and fixed array
	int array4[5] = { 9,7,5,3,1 };

	std::cout << sizeof(array4) << '\n';

	int* ptr4{ array4 };						// 20 (5 member * 4 byte)
	std::cout << sizeof(ptr4) << '\n' << '\n';	// 4  (1 member * 4 byte)



	return 0;

}