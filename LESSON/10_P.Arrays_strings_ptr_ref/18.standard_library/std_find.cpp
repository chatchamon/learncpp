#include <iostream>
#include <algorithm>
#include <array>

int main()
{
	std::array<int, 6> array{ 13,90,99,5,40,80 };
	std::cout << "Enter a valur to search and replace with: ";
	int search{};
	int replace;
	std::cin >> search >> replace;
	
	auto found{ std::find(array.begin(), array.end(), search) };

	if (found == array.end())
	{
		std::cout << "Could not find " << search << '\n';
		std::cout << "Array is \n";
	}
	else
	{
		*found = replace;
		std::cout << "element after replace\n";
	}
	
	for (int i : array)
		std::cout << i << ' ';
	std::cout << std::endl;

	return 0;
		
}