#include <algorithm>
#include <iostream>
#include <array>

void doubleNumber(int& i)
{
	i *= 2;
}

int main()
{
	std::array<int, 4> arr{ 1,2,3,4 };

	std::for_each(arr.begin(), arr.end(), doubleNumber);

	for (int i : arr)
		std::cout << i << ' ';
	std::cout << std::endl;

	return 0;
}