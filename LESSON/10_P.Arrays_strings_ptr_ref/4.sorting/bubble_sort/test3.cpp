﻿#include <algorithm>	// for std::swap
#include <iostream>
#include <iterator>		// for std::size

int main()
{
	//int array[]{ 1,2,3,4,5,6,7,8,9 };
	int array[]{6,3,2,9,7,1,5,4,8}; 
	int length = static_cast<int>(std::size(array));

std::cerr << "length is: " << length << '\n';

	for (int i{ 0 }; i < length; ++i)
		std::cout << array[i] << '\t';
	std::cout << '\n' << '\n';

	int count{0};

	//for total array
	for (int iteration{ 0 }; iteration < length - 1; ++iteration)
	{
		bool swapped{ false };		// if there is not swapped at that index: means it is the smallest member
		int endOfArrayIndex = length - iteration;		//อันทีเรียงไปเเล้วจะได้ไม่ต้องเรียงอีก

		// for each index
		for (int currentIndex{ 0 }; currentIndex < endOfArrayIndex - 1; ++currentIndex)
		{
			int nextIndex{ currentIndex + 1 };
			if (array[currentIndex] > array[nextIndex])
			{
				std::swap(array[currentIndex], array[nextIndex]);
				swapped = true;
				count += 1;
			}
		}
		if (!swapped)
		{
			std::cout << "Early iteration on iteration: " << iteration + 1 << '\n';
			break;
		}

	}
	

	for (int i{ 0 }; i < length; ++i)
		std::cout << array[i] << '\t';
	std::cout << '\n';

	std::cout << "amount of swapping: " << count << '\n';
	

	return 0;

}