﻿#include <algorithm> //for std::swap, use <utility> instead of C++11
#include <iostream>

int main()
{
	constexpr int length{ 5 };
	int array[length]{ 30,50,20,10,40 };

	for (int startIndex{ 0 }; startIndex < length - 1; ++startIndex)		// -1 เพราะว่ารอบสุดท้ายมันเเค่เทียบกับตัวมันเอง ไม่จำเป็น เสียเวลา
	{
		int smallestIndex{ startIndex };
		
		for (int currentIndex{ startIndex + 1 }; currentIndex < length; ++currentIndex)
		{
			if (array[currentIndex] < array[smallestIndex])
				smallestIndex = currentIndex;
		}
		std::swap(array[startIndex], array[smallestIndex]);
	}
	
	for (int index{ 0 }; index < length; ++index)
		std::cout << array[index] << ' ';
	std::cout << '\n';

	return 0;
	
}