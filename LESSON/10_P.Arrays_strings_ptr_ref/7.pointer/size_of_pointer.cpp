#include <iostream>

int main()
{
	char* chPtr;			// chars are 1 byte
	int* iPtr;				// ints are usually 4 byte
	struct Something
	{
		int nX, nY, nZ;
	};
	Something* somethingPtr;	// approximate 12 byte

	std::cout << sizeof(chPtr) << '\n';			// print 4
	std::cout << sizeof(iPtr) << '\n';			// print 4
	std::cout << sizeof(somethingPtr) << '\n';	// print 4

	//same size no matter what because it is a memory address

	return 0;

}