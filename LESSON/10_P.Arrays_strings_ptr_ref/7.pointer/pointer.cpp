#include <iostream>

int main()
{
	int x = 5;

	std::cout << x << '\n';				// 5
	std::cout << &x << '\n';			// 0027FEA0	(memory address)
	std::cout << *&x << '\n';			// 5		(dereference)
std::cout << std::endl;

	//pointer
	int v = 5;
	int* ptr = &v;

	std::cout << &v << '\n';		// print the address of the variable v
	std::cout << ptr << '\n';		// print the address that ptr is holding
std::cout << std::endl;

	//pointer and dereference
	int value = 5;
	std::cout << &value << '\n';
	std::cout << value << '\n';

	int* ptr1 = &value;
	std::cout << ptr1 << '\n';
	std::cout << *ptr1 << '\n';

std::cout << std::endl;

	// reassign a value to a pointer
	int input_1 = 5;
	int input_2 = 7;

	int* ptr2;

	ptr2 = &input_1;
	std::cout << *ptr2 << '\n';

	ptr2 = &input_2;
	std::cout << *ptr2 << '\n';

std::cout << std::endl;

	// reassign a value to a pointer w/ a dereference
	int assign = 5;
	int* ptr3 = &assign;

	*ptr3 = 7;
	std::cout << ptr3 << ' ' << *ptr3 <<  '\n';
std::cout << std::endl;

	return 0;
}