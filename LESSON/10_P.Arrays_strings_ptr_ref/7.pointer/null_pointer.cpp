#include <iostream>
#include <cstddef>

int main()
{
//case 1
	float* ptr{ 0 };		// ptr is now a null pointer

//case 2
	float ptr2;				// ptr2 is not initialized
	ptr2 = 0;				// ptr2 is now a null pointer

//case 3 (boolean)
	double* ptr3{ 0 };

	if (ptr)
		std::cout << "ptr3 is pointing to a double value.\n";
	else
		std::cout << "ptr3 is a null pointer.\n";		// print this case

	return 0;

//case 4 (include <cstddef>)
//however, avoid using this case
	double* ptr4{ NULL };	// ptr4 is now a null pointer





}