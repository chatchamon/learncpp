#include <iostream>
#include <cstddef>		// for NULL

void print(int x)
{
	std::cout << "print(int): " << x << '\n';
}

void print(int* x)
{
	if (!x)
		std::cout << "print(int*): null\n";
	else
		std::cout << "print(int*): " << *x << '\n';
}


int main()
{
	int* x{ NULL };
	print(x);			// print(int*) : null		// correct
	print(0);			// print(int) : 0			// correct
	print(NULL);		// print(int) : 0			// !!!incorrect

	int* y{ nullptr };
	print(y);			// print(int*) : null		// correct
	print(0);			// print(int) : 0			// correct
	print(nullptr);		// print(int*) : null		// correct

	return 0;
}