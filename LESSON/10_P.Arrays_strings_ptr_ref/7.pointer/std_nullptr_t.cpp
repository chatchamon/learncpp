#include <iostream>
#include <cstddef>		// for std::nullptr_t

void doSomething(std::nullptr_t ptr)
{
	std::cout << "in doSomething()\n";
}


int main()
{
	doSomething(nullptr);

	return 0;
}