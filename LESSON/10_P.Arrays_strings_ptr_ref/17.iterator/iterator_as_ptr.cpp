#include <iostream>
#include <array>
#include <iterator>

int main()
{
	std::array<int, 7> data{ 0,1,2,3,4,5,6 };

// write begin and end by ourself
	//auto begin{ &data[0] };
	//auto end{ &data[0] + std::size(data) }

// or use standard library
	//auto begin{ data.begin() };
	//auto end{ data.end() };

// or use #include <iterator>
	auto begin(std::begin(data));
	auto end(std::end(data));

	for (auto ptr{ begin }; ptr != end; ++ptr)
		std::cout << *ptr << ' ';
	std::cout << std::endl;

	return 0;
}