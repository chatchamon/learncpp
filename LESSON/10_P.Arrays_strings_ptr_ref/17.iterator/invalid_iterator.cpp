#include <iostream>
#include <vector>

int main()
{
	std::vector<int> v{ 1,2,3,4,5,6,7 };

	auto it{ v.begin() };

	++it;
	std::cout << *it << '\n';

	v.erase(it);

	++it;
	std::cout << *it << '\n';

	return 0;
}