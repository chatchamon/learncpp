#include <iostream>
#include <array>

int main()
{
	std::array<int, 7> data = { 0,1,2,3,4,5,6 };
	std::size_t length{ std::size(data) };
	
// while loop with explicit index
	std::size_t index{ 0 };
	while (index != length)
	{
		std::cout << data[index] << ' ';
		++index;
	}
	std::cout << std::endl;

// for loop with explicit index
	for (index = 0 ; index < length; ++index)
		std::cout << data[index] << ' ';
	std::cout << std::endl;

// for loop with pointer (ptr can't be const because we increment it)
	for (auto ptr{ &data[0] }; ptr != (&data[0] + length); ++ptr)
		std::cout << *ptr << ' ';
	std::cout << std::endl;

// ranged-based for loop
	for (int i : data)
		std::cout << i << ' ';
	std::cout << std::endl;


	return 0;
}