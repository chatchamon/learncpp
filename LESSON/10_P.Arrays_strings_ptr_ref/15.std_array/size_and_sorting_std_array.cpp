#include <iostream>
#include <array>
#include <algorithm>

void printLength(const std::array<double, 5>& myArray)
{
	std::cout << "length: " << myArray.size() << '\n';
}

void printElement(const std::array<double, 5>& myarray)
{
	for (auto element : myarray)
		std::cout << element << '\t';
	std::cout << std::endl;
}

int main()
{
	//c++ 17 compatible
	//std::array myArray{9.0,7.2,5.4,3.6,1.8};

	std::array<double, 5> myArray{ 9.0,7.2,5.4,3.6,1.8 };
	
	//print lenght
	printLength(myArray);

	//print element
	printElement(myArray);

	//sort elements + print out
	std::sort(myArray.begin(), myArray.end());
	printElement(myArray);


	return 0;

}