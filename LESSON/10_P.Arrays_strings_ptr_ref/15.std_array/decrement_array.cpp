#include <iostream>
#include <array>

int main()
{
	std::array<int, 5> myArray = { 7,3,1,9,5 };

	for (auto i{ myArray.size() }; i-- > 0;)
		std::cout << myArray[i] << ' ';
	std::cout << '\n';

	return 0;

}
