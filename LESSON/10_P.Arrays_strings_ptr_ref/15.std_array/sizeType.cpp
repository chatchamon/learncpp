#include <array>
#include <iostream>

int main()
{
	//C++17 compatible
	std::array myArray{ 7,3,1,9,5 };

// method 1
	using index_t = std::array<int, 5>::size_type;
	for (index_t i{0}; i < myArray.size(); ++i)
		std::cout << myArray[i] << ' ';
	std::cout << std::endl;

// method 2
	for (std::size_t i{ 0 }; i < myArray.size(); ++i)
		std::cout << myArray[i] << ' ';
	std::cout << std::endl;


	return 0;
}