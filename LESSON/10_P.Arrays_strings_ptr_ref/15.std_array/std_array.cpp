#include <iostream>
#include <array>

int main()
{
// declare an array 
	std::array<int, 3> myArray;

// initializa an array
	std::array<int, 5> myArray2 = { 9,7,5,3,1 };
	std::array<int, 5> myArray3{9, 7, 5, 3, 1};

// initialize an array for c++17
	std::array myArray4 = { 9,7,5,3,1 };	// deduced <int, 5>
	std::array myArray5{ 9.7, 7.31 };		// deduced <double, 2>

// assign value
	std::array<int, 5> myArray6;
	myArray6 = { 0,1,2,3,4 };				// okay {0,1,2,3,4}
	myArray6 = { 9,8,7 };					// okay {0,1,2,0,0}
	//myArray6 = { 0,1,2,3,4,5,6 };			// not allowed, too much elements

// another element access, at()
	std::array myArray7{ 9,7,5,3,1 };
	myArray7.at(1) = 10;					// okay {9,10,5,3,1}
	myArray7.at(9) = 6;						// throw an error!, no element 9 here

	return 0;
}