#include <iostream>

int main()
{
	int value{ 5 };
	void* voidPtr{ &value };

	//std::cout << *voidPtr << '\n';			// illegal

	//void ptr cannot dereference directly
	int* intPtr{ static_cast<int*>(voidPtr) };
	std::cout << *intPtr << '\n';


	return 0;
}