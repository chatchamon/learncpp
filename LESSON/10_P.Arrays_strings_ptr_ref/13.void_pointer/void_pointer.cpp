#include <iostream>

int main()
{
	int nValue;
	float fValue;

	struct Something
	{
		int n;
		float f;
	};

	Something sValue;

	void* ptr;
	ptr = &nValue;		// valid
	ptr = &fValue;		// valid
	ptr = &sValue;		// valid


	return 0;

}