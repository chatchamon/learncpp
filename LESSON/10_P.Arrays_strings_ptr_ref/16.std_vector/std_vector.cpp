#include <vector>

int main()
{
// declare vector: dynamic allocated memory, no need of state the length
	std::vector<int> array;
	std::vector<int> array2 = { 9,5,7,3,1 };
	std::vector<int> array3{ 9,5,7,3,1 };

	//C++17 compatible
	std::vector array4{ 9,5,7,3,1 };

// access array element
	array[6] = 2;				// no bound checking
	array.at(7) = 3;			// bound checking
		
// assign value
	array = { 0,1,2,3,4 };		// okay, length = 5;
	array = { 9,8,7 };			// okat, length = 3;


}