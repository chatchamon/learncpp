#include <vector>
#include <iostream>

int main()
{
	//std::vector<int> array{ 0,1,2,3,4 };
	//array.resize(3);
	
	std::vector<int> array(5);

	std::cout << "The length is " << array.size() << '\n';

	for (int i : array)
		std::cout << i << ' ';
	std::cout << std::endl;

	return 0;
}