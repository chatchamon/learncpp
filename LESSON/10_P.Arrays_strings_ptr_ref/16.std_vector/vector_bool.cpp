#include <iostream>
#include <vector>

int main()
{
	std::vector<bool> array = { true, false, true, true, false };

	std::cout << "The length is " << array.size() << '\n';

	for (int i : array)
		std::cout << i << '\t';

	std::cout << std::endl;

	return 0;
}