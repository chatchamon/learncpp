#include <iostream>
#include <string>

namespace myAnimals
{
	enum Animals
	{
		CHICKEN,
		DOG,
		CAT,
		ELEPHANT,
		DUCK,
		SNAKE,
		MAX_ANIMAL,
	};
}

namespace myAnimalLeg
{
	static int array[myAnimals::MAX_ANIMAL] = { 2,4,4,4,2,0 };
}

std::string getAnAnimalNames(myAnimals::Animals animal)
{
	switch (animal)
	{
	case(myAnimals::CHICKEN): std::cout << "Chicken"; return "Chicken"; break;
	case(myAnimals::DOG): std::cout << "Dog"; return "Dog"; break;
	case(myAnimals::CAT): std::cout << "Cat"; return "Cat"; break;
	case(myAnimals::ELEPHANT): std::cout << "Elephant"; return "Elephant"; break;
	case(myAnimals::DUCK): std::cout << "Duck"; return "Duck"; break;
	case(myAnimals::SNAKE): std::cout << "Snake"; return "Snake"; break;
	}
}

void getAorAn(myAnimals::Animals animal)
{
	std::string name = getAnAnimalNames(animal);

	//get only the first letter
	char first_letter = name[0];

	if (first_letter == 'A' || first_letter == 'E' || first_letter == 'I' || first_letter == 'O' || first_letter == 'U')
		std::cout << "An ";
	else
		std::cout << "A ";

	name.clear();

	/*switch (first_letter)
	{
	
	case(myAnimals::ELEPHANT): std::cout << "An "; break;
	default: std::cout << "A "; break;
	}
	*/
	
}

void printName(myAnimals::Animals animal)
{
	getAorAn(animal);
	getAnAnimalNames(animal);
	std::cout << " has " << myAnimalLeg::array[animal] << " legs.\n";
}
	

int main()
{
	//double temperature[365] = {};

	//int array[myAnimals::MAX_ANIMAL] = { 2,4,4,4,2,0 };
	//myAnimalLeg::array;

	printName(myAnimals::CHICKEN);
	//printName(myAnimals::DOG);
	//printName(myAnimals::CAT);
	printName(myAnimals::ELEPHANT);
	//printName(myAnimals::DUCK);
	//printName(myAnimals::SNAKE);



	// dumb assignment
	/*array[myAnimals::CHICKEN] = 2;
	array[myAnimals::DOG] = 4;
	array[myAnimals::CAT] = 4;
	array[myAnimals::ELEPHANT] = 4;
	array[myAnimals::DUCK] = 2;
	array[myAnimals::SNAKE] = 0;
	*/

	// long version printing
	/*getAorAn(myAnimals::CHICKEN);
	getAnAnimalNames(myAnimals::CHICKEN);
	std::cout << " has " << array[myAnimals::CHICKEN] << " legs." << '\n';
	*/
	
	

	return 0;
}