#include <iostream>
#include <string>
#include "animal_leg.h"

std::string getAnAnimalNames(myAnimals::Animals animal)
{
	switch (animal)
	{
	case(myAnimals::CHICKEN): return "Chicken"; break;
	case(myAnimals::DOG): return "Dog"; break;
	case(myAnimals::CAT): return "Cat"; break;
	case(myAnimals::ELEPHANT): return "Elephant"; break;
	case(myAnimals::DUCK): return "Duck"; break;
	case(myAnimals::SNAKE): return "Snake"; break;
	}
}

void getAorAn(myAnimals::Animals animal)
{
	std::string name = getAnAnimalNames(animal);

	//get only the first letter
	char first_letter = name[0];

	if (first_letter == 'A' || first_letter == 'E' || first_letter == 'I' || first_letter == 'O' || first_letter == 'U')
		std::cout << "An ";
	else
		std::cout << "A ";

	name.clear();
}

void printName(myAnimals::Animals animal)
{
	getAorAn(animal);
	std::cout << getAnAnimalNames(animal) << " has "
		<< myAnimalLeg::array[animal] << " legs.\n";
}