#ifndef ANIMAL_LEG_H
#define ANIMAL_LEG_H

namespace myAnimals
{
	enum Animals
	{
		CHICKEN,
		DOG,
		CAT,
		ELEPHANT,
		DUCK,
		SNAKE,
		MAX_ANIMAL,
	};
}

namespace myAnimalLeg
{
	static int array[myAnimals::MAX_ANIMAL] = { 2,4,4,4,2,0 };
}

// neccessary function in the program
std::string getAnAnimalNames(myAnimals::Animals animal);
void getAorAn(myAnimals::Animals animal);
void printName(myAnimals::Animals animal);

#endif