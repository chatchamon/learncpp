#include <iostream>
#include <string>
#include "animal_leg.h"

int main()
{
	printName(myAnimals::CHICKEN);
	printName(myAnimals::DOG);
	printName(myAnimals::CAT);
	printName(myAnimals::ELEPHANT);
	printName(myAnimals::DUCK);
	printName(myAnimals::SNAKE);

	return 0;
}