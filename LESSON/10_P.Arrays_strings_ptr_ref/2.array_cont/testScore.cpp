#include <iostream>

namespace MyStudents
{
	enum StudentNames
	{
		IMM,
		OR,
		MA,
		Day,
		MAX_STUDENTS,
	};
}

int main()
{
	int testScores[MyStudents::MAX_STUDENTS];
	testScores[MyStudents::IMM] = 72;

	std::cout << testScores[0] << '\n';
	std::cout << testScores[MyStudents::IMM] << '\n';

	return 0;
}