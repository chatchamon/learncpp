#include <iostream>

// array size	= array length (no. of member)	* pointer size (int 4 byte)
// 32 byte		= 8 member						* 4 byte

void printSize(int array[])
{
	std::cout << sizeof(array) << '\n';		// prints the size of a pointer, not the size of the array
}

int main()
{
	int array[] = { 1,1,2,3,5,8,13,21 };
	std::cout << sizeof(array) << '\n';		// will print the size of the array (int 4 byte)
	
	printSize(array);
	std::cout << std::endl;

	std::cout << sizeof(array) / sizeof(array[0]) << '\n';	// will print amount of members = array length

	return 0;
}