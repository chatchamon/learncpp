#include <iostream>
#include <string>

int main()
{
// fibonacci example
	constexpr int fibonacci[]{ 0,1,1,2,3,5,8,13,21,34,55,89 };

	//for (int number : fibonacci)
	for (auto number : fibonacci)
		std::cout << number << ' ';
	std::cout << std::endl;

// string example
	std::string array[]{ "Peter", "likes", "frozen", "yogurt" };
	for (auto element : array)
		std::cout << element << ' ';
	std::cout << std::endl;

// string example: not copying all element, using ref
	std::string array1[]{ "Peter", "likes", "frozen", "yogurt" };
	for (auto& element1 : array1)							// ref
		std::cout << element1 << ' ';
	std::cout << std::endl;

// string example: read-only fashion, using const
	std::string array2[]{ "Peter", "likes", "frozen", "yogurt" };	
	for (const auto& element2 : array2)
		std::cout << element2 << ' ';
	std::cout << std::endl;


	return 0;
}