#include <iostream>

int main()
{
	int array[]{ 4,6,7,3,8,2,1,9,5 };
	int size = static_cast<int>(std::size(array));

	while (true)
	{
		std::cout << "Please enter a number between 1 to 9: ";
		int x{};
		std::cin >> x;

		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(32767, '\n');
		}
		else if (x == 1 || x == 2 || x == 3 || x == 4 || x == 5 || x == 6 || x == 7 || x == 8 || x == 9)
		{
			for (int i{ 0 }; i < size; ++i)
				std::cout << array[i] << '\t';
			std::cout << '\n';
			break;
		}
	}
	
	return 0;

}