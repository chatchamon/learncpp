#include <iostream>

int main()
{
	int number{};

	do
	{
		std::cout << "Enter a number between 1 to 9: ";
		std::cin >> number;

		//if the user enter an invalid character
		if (std::cin.fail())				
			std::cin.clear();
		std::cin.ignore(32767, '\n');

	} while (number < 1 || number > 9);		// will run until the input number is not out of range

	int array[]{ 4,6,7,3,8,2,1,9,5 };
	int arrayLength = static_cast<int>(std::size(array));

	//searching through array to find the number inside the array
	for (int index{ 0 }; index < arrayLength; ++index)
	{
		if (array[index] == number)
		{
			std::cout << "The number " << number << " is at index " << index << '\n';
			break;
		}
	}

	return 0;
}
