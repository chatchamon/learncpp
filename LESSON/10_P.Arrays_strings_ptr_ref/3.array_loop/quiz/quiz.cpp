#include <iostream>
#include <iterator> // for std::size

int main()
{
    int scores[]{ 84, 92, 76, 81, 56 };
    int numStudents{ static_cast<int>(std::size(scores)) };

    int maxScore{ 0 }; // keep track of our largest score

    // now look for a larger score
    int maxIndex{ 0 };
    for (int student{ 0 }; student < numStudents; ++student)
        if (scores[student] > maxScore)
        {
            maxScore = scores[student];
            maxIndex = student;
        }

    std::cout << "The best score was " << maxScore << " from student " << maxIndex << '\n';

    return 0;
}