#include <iostream>

int main()
{
	int scores[] = { 84,92,76,81,56 };
	int numStudents{ static_cast<int>(std::size(scores)) };
	int totalScore{ 0 };
	int maxScore{ 0 };

	for (int student{ 0 }; student < numStudents; ++student)
		totalScore += scores[student];

	auto averageScore{ static_cast<double>(totalScore) / numStudents };
	
	for (int student{ 0 }; student < numStudents; ++student)
		if (scores[student] > maxScore)
			maxScore = scores[student];

	std::cout << "Total Score: " << totalScore << '\n';
	std::cout << "Number of students: " << numStudents << '\n';
	std::cout << "Average score: " << averageScore << '\n';
	std::cout << "The best score is: " << maxScore << '\n';

	return 0;

}