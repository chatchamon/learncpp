#include <iostream>

//Rule: Pass non - pointer, non - fundamental data type variables(such as structs) 
//by(const) reference.

void printIt(const int& x)
{
	std::cout << x << '\n';
}

int main()
{
	int a = 1;
	printIt(a);				// non-const l-valued

	const int b = 2;
	printIt(b);				// const l-valued

	printIt(3);				// literal r-valued

	printIt(2 + b);			// expression r-valued

	return 0;

}