#include <iostream>

void changeN(int& ref)
{
	ref = 6;
}

int main()
{
	int n{ 5 };
	std::cout << n << '\n';		// 5

	changeN(n);
	std::cout << n << '\n';		// 6

	return 0;
}