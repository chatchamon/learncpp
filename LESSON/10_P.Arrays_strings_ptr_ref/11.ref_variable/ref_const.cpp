#include <iostream>

int main() 
{
// a const ref
	const int value = 5;
	const int& ref = value;

// a const ref initialize (non-const l-valued, const l-valued, r-valued)
	int x = 5;
	const int& ref1 = x;		// x is a non-const l-valued

	const int y = 7;
	const int& ref2 = y;		// y is a const l-valued
			
	const int& ref3 = 6;		// 6 is r-valued		

}