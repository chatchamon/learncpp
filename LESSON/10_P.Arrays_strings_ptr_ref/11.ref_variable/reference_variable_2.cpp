#include <iostream>

int main()
{
// reference must be initialized
	int value = 5;
	int& ref{ value };
	int& invalidRef{};

// reference to a non-const value with non-con l-valued: not const, not r-valued
	int x{ 5 };
	int& ref1{ x };			// ref1 = x

	const int y{ 7 };
	int& ref2{ y };			// not okay, y is a const-value

	int& ref3{ 6 };			// not okay, 6 is a r-valued

// ref cannot be reassigned
	int value1{ 5 };
	int value2{ 6 };

	int& ref{ value1 };		// ref = value1 
	ref = value2;			// assign value2 to value 1: does not change the ref

}