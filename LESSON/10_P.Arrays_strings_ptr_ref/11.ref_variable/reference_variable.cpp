#include <iostream>

int main()
{
	int value{ 5 };
	int& ref{ value };

	value = 6;
	ref = 7;

	std::cout << value << '\n';		// 7
	++ref;
	std::cout << value << '\n';		// 8

	std::cout << &value << '\n';	// 00D3F97C
	std::cout << &ref << '\n';		// 00D3F97C

	return 0;
}