#include <iostream>

int main() 
{
	const int value = 5;
	const int* ptr = &value;

// pointer can redirect
	int value1 = 5;
	const int* ptr1 = &value1;

	int value2 = 6;
	ptr1 = &value2;

// constant pointer
	int value3 = 5;
	int* const ptr3 = &value3;		// pointer will point at the same address

// another example of constant pointer
	int value4 = 5;
	int* const ptr4 = &value4;
	*ptr4 = 6;

// constant pointer to a constant value
	int value5 = 5;
	const int* const ptr5 = &value;

// conclusion: type of pointer						// ex: int value = 5;
	// 1. a non-const pointer						// int							ptr1	= &value;
	// 2. a pointer to a const value				// const	int					*ptr2	= &value;
	// 2. a const pointer							//	int				*const		ptr3	= &value;
	// 3. a const pointer to a const value			// const	int		*const		ptr4	= & value;

	return 0;

}