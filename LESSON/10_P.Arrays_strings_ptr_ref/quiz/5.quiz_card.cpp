#include <iostream>
#include <string>
#include <array>
#include <algorithm>		// for std::shuffle
#include <ctime>
#include <random>

enum class Ranks
{
	TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN , KING, ACE,
	MAX_RANK,
};

enum class Suits
{
	CLUBS, DIAMONDS, HEARTS, SPADES,
	MAX_SUIT,
};

struct Card
{
	Ranks rank{};
	Suits suit{};
};

void printCard(const Card &card)
{
	switch (card.rank)
	{
	case Ranks::TWO: std::cout << 2; break;
	case Ranks::THREE: std::cout << 3; break;
	case Ranks::FOUR: std::cout << 4; break;
	case Ranks::FIVE: std::cout << 5; break;
	case Ranks::SIX: std::cout << 6; break;
	case Ranks::SEVEN: std::cout << 7; break;
	case Ranks::EIGHT: std::cout << 8; break;
	case Ranks::NINE: std::cout << 9; break;
	case Ranks::TEN: std::cout << 10; break;
	case Ranks::JACK: std::cout << 'J'; break;
	case Ranks::QUEEN: std::cout << 'Q'; break;
	case Ranks::KING: std::cout << 'K'; break;
	case Ranks::ACE: std::cout << 'A'; break;
	default: std::cout << '?'; break;
	}
	switch (card.suit)
	{
	case Suits::CLUBS: std::cout << 'C'; break;
	case Suits::DIAMONDS: std::cout << 'D'; break;
	case Suits::HEARTS: std::cout << 'H'; break;
	case Suits::SPADES: std::cout << 'S'; break;
	default: std::cout << '?'; break;
	}
}

using array_type = std::array<Card, 52>;
using index_type = std::array<Card, 52>::size_type;

array_type createDeck()
{
	array_type deck{};

	index_type card{ 0 };

	index_type max_suit = static_cast<index_type>(Suits::MAX_SUIT);
	index_type max_rank = static_cast<index_type>(Ranks::MAX_RANK);

	for (int suit{0}; suit < max_suit; ++suit)
		for (int rank{ 0 }; rank < max_rank; ++rank)
		{
			deck[card].suit = static_cast<Suits>(suit);
			deck[card].rank = static_cast<Ranks>(rank);
			++card;
		}
	return deck;
}

void printDeck(const array_type& deck)
{
	for (auto &element : deck)
	{
		printCard(element);
		std::cout << '\t';
	}
	std::cout << '\n';
}

array_type shuffleDeck(array_type deck)
{
	std::mt19937 mt{ static_cast<std::mt19937::result_type>(std::time(nullptr)) };
	std::shuffle(deck.begin(), deck.end(), mt);

	return deck;
}

int getCardValue(const Card &card)
{
	if (static_cast<int>(card.rank) < 9)
	{
		return static_cast<int>(card.rank) + 2;
	}
	switch (card.rank)
	{
	case Ranks::JACK:
	case Ranks::QUEEN:
	case Ranks::KING:
		return 10; break;
	case Ranks::ACE: return 11; break;
	}
	return 0;
}



int main()
{
	Card card{ Ranks::ACE, Suits::CLUBS };
	Card card2{ Ranks::TWO, Suits::SPADES };

	printCard(card);
	std::cout << '\n';

	array_type deck{ createDeck() };

	printDeck(deck);
	printDeck(shuffleDeck(deck));
	
	std::cout << '\n';
	std::cout << getCardValue(card);
	std::cout << getCardValue(card2);

	return 0;
}

