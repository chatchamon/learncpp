#include <iostream>
#include <array>
#include <numeric>		// for std::accumulate

enum ItemType
{
	HEALTH_POTIONS, TORCHES, ARRAOWS, MAX_ITEMS,
};

int countTotalItems(std::array<int, ItemType::MAX_ITEMS> array)
{
	return std::accumulate(array.begin(), array.end(), 0);
}

int main()
{
	std::array<int, ItemType::MAX_ITEMS> user = { 2,5,10 };

	std::cout << "The player has " << countTotalItems(user) << " item(s)\n";
	std::cout << "The player has " << user[ItemType::TORCHES] << " torch(es)\n";

	return 0;
}