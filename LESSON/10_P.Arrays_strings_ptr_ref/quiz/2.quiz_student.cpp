#include <iostream>
#include <string>
#include <vector>
#include <algorithm>		// for std::sort

struct StudentAndScore
{
	std::string StudentName;
	int Score{};
};

bool compareScore(const StudentAndScore& a, const StudentAndScore& b)
{
	return a.Score > b.Score;
}

int main()
{
	// ask user how many students they want to enter
	std::cout << "How many student? ";
	int length;
	std::cin >> length;

	// create a vector to hold all student
	std::vector<StudentAndScore> student;
	student.resize(length);

	for (int index{ 0 }; index < length; ++index)
	{
		std::cout << "Name: ";
		std::cin >> student[index].StudentName;
		std::cout << "Score: ";
		std::cin >> student[index].Score;
	}

	int index{ 0 };
	std::sort(student.begin(), student.end(), compareScore);

	std::cout << '\n';
	for (int index{ 0 }; index < length; ++index)
	{
		std::cout << student[index].StudentName << " got a grade of " 
			<< student[index].Score << '\n';
	}
	





	

	return 0;
}