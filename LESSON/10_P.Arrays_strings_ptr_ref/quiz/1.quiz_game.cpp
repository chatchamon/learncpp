#include <numeric>		// for std::reduce
#include <array>
#include <iostream>

enum ItemTypes 
{
	HEALTH_POTION, TORCHES, ARROWS, MAX_ITEMS
};

int countTotalItem(std::array<int, ItemTypes::MAX_ITEMS> items)
{
	return std::reduce(items.begin(), items.end());
}

int main()
{
	std::array<int, ItemTypes::MAX_ITEMS> user{ 2,5,10 };

	std::cout << "The player has " << countTotalItem(user) << " item(s) in total\n";

	std::cout << "The player has " << user[ItemTypes::TORCHES] << " torch(es)\n";

	return 0;
}