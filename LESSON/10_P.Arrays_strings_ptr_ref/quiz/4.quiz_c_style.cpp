#include <iostream>

int main()
{
	const char* myString { "Hello World!" };

	const char* ptr = &myString[0];

	while (*ptr != '\0')
	{
		std::cout << *ptr;
		++ptr;
	}

	std::cout << '\n';

	return 0;
}