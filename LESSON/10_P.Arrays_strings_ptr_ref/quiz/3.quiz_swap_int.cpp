#include <iostream>

int getUserInt()
{
	std::cout << "Enter an integer: ";
	int input{};
	std::cin >> input;

	return input;
}


void swapInt(int &a, int &b)
{
	int temp{a};
	a = b;
	b = temp;
}

int main()
{
	int input1{ getUserInt() };
	int input2{ getUserInt() };

	swapInt(input1, input2);

	std::cout << input1 << ' ' << input2 << '\n';

	return 0;


}