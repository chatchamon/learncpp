#include <iostream>
#include <string_view>
#include <cstring>

int main()
{
	std::string_view str{ "balloon" };
	str.remove_prefix(1);		// prefix and suffix don't modify the string, 
	str.remove_suffix(3);		// just change the region of observing

	std::cout << str << " has " << std::strlen(str.data()) << " letters.\n";
	std::cout << "str.data() is " << str.data() << '\n';
	std::cout << "str is " << str << '\n';

	return 0;
}