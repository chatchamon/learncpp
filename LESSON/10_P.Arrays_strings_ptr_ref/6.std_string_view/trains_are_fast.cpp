#include <iostream>
#include <string_view>

int main()
{
	std::string_view str{ "Trains are fast!" };

	std::cout << "str: " << str << '\n';
	std::cout << "str.length: " <<str.length() << '\n';

	std::cout << "subtrating (0, str.find(' ')): " << str.substr(0, str.find(' ')) << '\n';
	std::cout << "str after subtracting: " << str << '\n';
	std::cout << (str == "Trains are fast!") << '\n';		//1 ( which means true)

	return 0;
}