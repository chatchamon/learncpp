#include <iostream>
#include <string_view>
#include <string>

void print(std::string s)
{
	std::cout << s << '\n';
}


int main()
{
	std::string_view sv{ "balloon" };
	
	sv.remove_suffix(3);
	//print(sv);				// error; cannot compile

	std::string str{ sv };		// explicit conversion
	print(str);									// okay

	print(static_cast<std::string>(sv));		// okay

	return 0;


}