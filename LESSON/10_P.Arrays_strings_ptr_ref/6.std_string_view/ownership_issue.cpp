#include <iostream>
#include <string_view>

std::string_view askForName()
{
	std::cout << "What is your name? \n";
	std::string str{};
	std::cin >> str;

	std::string_view view{ str };
	std::cout << "Hello " << view << '\n';

	return view;
}// view is out of scope here


int main()
{
	std::string_view view{ askForName() };
	std::cout << "Your name is " << view << '\n';

	return 0;
}