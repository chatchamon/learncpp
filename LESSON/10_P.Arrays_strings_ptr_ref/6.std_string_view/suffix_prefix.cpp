#include <iostream>
#include <cstring>	// for std::strlen
#include <string_view>

//actually it is the same 'Peach', but we lost sight of it, and couldn't bring back

int main()
{
	std::string_view str{ "Peach" };
	std::cout << str << '\n';				// Peach

	str.remove_prefix(1);
	std::cout << str << '\n';				// each

	str.remove_suffix(2);
	std::cout << str << '\n';				// ea

	return 0;

}