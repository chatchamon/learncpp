#include <string_view>
#include <string>
#include <cstring>
#include <iostream>

int main()
{
	std::string_view sv{ "balloon" };

	sv.remove_suffix(3);

	std::string str{ sv };
	
	auto szNullTerminated{ str.c_str() };

	std::cout << str << " has " << std::strlen(szNullTerminated) << " letters\n";

	return 0;
}