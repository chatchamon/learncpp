#include <iostream>
#include <string_view>

// bad example of malmanipulate string_view
// because intention of string_view adoptation is to not copy the value; just to view

int main()
{
	char arr[]{ "Gold" };
	std::string_view str{ arr };

	std::cout << str << '\n';

	//change 'd' to 'f' in arr
	arr[3] = 'f';

	std::cout << str << '\n';

	return 0;

}