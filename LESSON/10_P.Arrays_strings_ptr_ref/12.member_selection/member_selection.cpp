#include <iostream>

struct Person
{
	int age;
	double weight;
};

int main()
{
	Person person;

//access member with actual struct
	person.age = 5;

//access member with ref
	Person& ref = person;
	ref.age = 5;

//access member with pointer
	Person* ptr = &person;
	(*ptr).age = 5;					// must have parenthesis for higher order

	//or
	ptr->age = 5;					// avoid precedence error

}