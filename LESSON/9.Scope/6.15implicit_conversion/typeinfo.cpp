#include <iostream>
#include <typeinfo> //for typeid

int main()
{
	short a{ 4 };
	short b{ 5 };
	std::cout << typeid(a + b).name() << " " << a + b << '\n';

	double d{ 4.2 };
	short s{ 2 };
	std::cout << typeid(d + s).name() << " " << d + s << '\n';

	return 0;
}