#include <iostream>
#include <iomanip>

int main()
{
	//showing implicit conversion
	double d{ 3 };
	short s{ 2 };
	std::cout << "double d = " << d << '\n';
	std::cout << "short s = " << s << '\n';

	//showing overflow and type conversion
	int i{ 30000 };
	char c = i;
	std::cout << static_cast<int>(c) << '\n';

	//showing bigger to smaller type is fine when it fits in smaller type's range
	int a{ 2 };
	short sh = a;
	std::cout << sh << '\n';

	double b{ 0.1234 };
	float f = b;
	std::cout << f << '\n';

	//floating number may loss precision in the smaller type
	float fl = 0.123456789; //double 0.123456789 has 9 significant digits, but float can hold to 7 digits
	std::cout << std::setprecision(9) << fl << '\n';

	//converting from int to float should be fine, under the range of float
	int e{ 10 };
	float g = e;
	std::cout << g << '\n';

	//converting from float to int is fine under int's range, but lost fractional point
	int h = 3.5;
	std::cout << h << '\n';			//called narrowing conversions

	//double to int is no, cannot fit int's range
	/*double k{ 10.0 };
	int m{ d };
	std::cout << m << '\n';*/

	return 0;
}