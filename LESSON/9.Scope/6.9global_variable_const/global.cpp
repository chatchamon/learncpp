#include <iostream>

namespace constants
{
	static const double gravity{ 9.8 };
}

/*double getGravity()
{
	return constants::gravity;
}*/


double instantVelocity(int time, double gravity)
{
	return time * gravity;
}

int main()
{
	std::cout << instantVelocity(5, constants::gravity) << '\n';

	return 0;
}