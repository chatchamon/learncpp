#include<iostream>

int add(int x, int y)
{
	return x + y;
}

int main()
{
	auto sum{ add(5,6) };

	using std::cout;
	cout << "add 5 and 6: " << add(5,6) << '\n';
	cout << "sum: " << sum << '\n';
	return 0;
}