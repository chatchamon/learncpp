#include<iostream>

void incrementAndPrint()
{
	static int s_x{};
	++s_x;
	std::cout << s_x << '\n';
}


int main()
{
	incrementAndPrint();
	incrementAndPrint();
	incrementAndPrint();
	incrementAndPrint();

	return 0;
}