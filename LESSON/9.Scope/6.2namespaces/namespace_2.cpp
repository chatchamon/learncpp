#include <iostream>

namespace foo
{
	namespace goo
	{
		int add(int x, int y)
		{
			return x + y;
		}
	}
}

int main()
{
	namespace boo = foo::goo; //alias namespace
	std::cout << boo::add(3, 7);

	return 0;
}