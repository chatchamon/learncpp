#include <iostream>

int main()
{
	char c{ 'a' };
	std::cout << static_cast<int>(c) << '\n';

	int i1{ 10 };
	int i2{ 4 };
	float f{ static_cast<float>(i1) / i2 };
	std::cout << f << '\n';

	int i{ 48 };						//int (48) = char (0)
	char a = i;							//implicit conversion -> unsafe
	char b = static_cast<char>(i);		//explicit conversion(static_cast)
	std::cout << "a " << a << ' ' << "b " << b << '\n';

	int in1{ 100 };
	in1 = in1 / 2.5;					//implicit conversion
	std::cout << in1 << '\n';

	int in2{ 100 };
	in2 = static_cast<int>(in2 / 2.5);	//explicit conversion
	std::cout << in2 << '\n';


	return 0;
}