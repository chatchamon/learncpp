#include <iostream>

int cout()
{
	return 5;
}

int main()
{
	//using namespace std;					//ambiguous name // foul usage
	using std::cout;
	cout << "Hello world!\n";		

	return 0;
}