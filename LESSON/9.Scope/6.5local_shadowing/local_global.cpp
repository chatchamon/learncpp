#include <iostream>

int value{ 5 };

void foo()
{
	std::cout << "global variable: " << value << '\n';
}

int main()
{
	int value{ 7 };
	++value;

	std::cout << "Local variable: " << value << '\n';

	foo();

	return 0;
}