#include <iostream>

int main()
{
	std::cout << "Enter an integer: ";
	int x{};
	std::cin >> x;

	std::cout << "Enter a larger interger: ";
	int y{};
	std::cin >> y;

	std::cout << "Swapping the values\n";

	if (x > y)
	{
		int temp{};
		temp = x;
		x = y;
		y = temp;
	}
	
	std::cout << "the smaller value is " << x << '\n';
	std::cout << "the larger value is " << y << '\n';

	/*
	if (x < y)
	{
		x = y;
	}
	else
	{
		std::cout << "the smaller value is " << y << '\n';
		std::cout << "the larger value is " << x << '\n';
	}*/

	return 0;
}