#include <iostream>
#include <string>
#include <array>
#include <string_view>
#include <ctime>
#include <cstdlib>

class Monster
{
public:
	enum Type
	{
		DRAGON, GOBLIN, ORGE, ORC, SKELETON, TROLL, VAMPIRE, ZOMBIE, MAX_MONSTER_TYPES,
	};
private:
	Type m_type;
	std::string m_name;
	std::string m_roar;
	int m_hitPoint;

public:
	Monster(Type type,const std::string& name,const std::string& roar, int hitPoint)
		:m_type{ type }, m_name{ name }, m_roar{ roar }, m_hitPoint{hitPoint}
	{
	}

	std::string getTypeString() const
	{
		switch (m_type)
		{
		case DRAGON: return "dragon";
		case GOBLIN: return "goblin";
		case ORGE: return "orge";
		case ORC: return "orc";
		case SKELETON: return "skeleton";
		case TROLL: return "troll";
		case VAMPIRE: return "vampire";
		case ZOMBIE: return "zombie";
		}
	}

	void print() const
	{
		std::cout << m_name << " the " << getTypeString() << " has " << m_hitPoint
			<< " hit points and says " << m_roar << '\n';
	}
};

class MonsterGenerator
{
public:
	static int getrandonNumber(int min, int max)
	{
		static constexpr double fraction{ 1.0 / (static_cast<double>(RAND_MAX) + 1.0) };
		return min + static_cast<int>((max - min + 1) * (std::rand() * fraction));
	}

	static Monster generateMonster()
	{
		auto type{ static_cast<Monster::Monster::Type>(getrandonNumber(0, Monster::MAX_MONSTER_TYPES - 1)) };
		int hitPoint{ getrandonNumber(1, 100) };

		static std::array<std::string_view, 6> s_name {"a", "b", "c", "d", "e", "f"};
		static std::array<std::string_view, 6> s_roar{ "*a*", "*b*", "*c*", "*d*", "*e*", "*f*" };

		std::string name{ s_name[(getrandonNumber(0, s_name.size() - 1))] };
		std::string roar{ s_roar[getrandonNumber(0, s_roar.size() - 1)] };

		return{ type, name, roar, hitPoint };
	}

};


int main()
{
	//Monster skeleton{ Monster::SKELETON, "Bones", "*rattle*", 4 };
	//skeleton.print();

	std::srand(static_cast<unsigned int>(std::time(nullptr)));
	//std::srand();

	Monster m{ MonsterGenerator::generateMonster() };
	m.print();

	return 0;
}