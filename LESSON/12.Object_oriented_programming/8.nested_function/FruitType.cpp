#include <iostream>

class Fruit
{
public:
	enum FruitType
	{
		APPLE, BANANA, LEMON,
	};

private:
	FruitType m_type;
	int m_percentageEaten;

public:
	Fruit(FruitType type)
		:m_type{ type }
	{
	}

	FruitType getType() { return m_type; }
	int getPercentageEaten() { return m_percentageEaten; }
};

int main()
{
	Fruit apple(Fruit::APPLE);
	if (apple.getType() == Fruit::APPLE)
		std::cout << "I am an apple\n";
	else
		std::cout << "I am not an apple\n";

	return 0;
}