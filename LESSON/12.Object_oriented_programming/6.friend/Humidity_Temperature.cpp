#include <iostream>

class Temperature;

class Humidity
{
private:
	int m_humid;
public:
	Humidity(int humid=0) { m_humid = humid; }
	friend void printWeather(const Temperature& temp, const Humidity& humid);
};

class Temperature
{
private:
	int m_temp;
public:
	Temperature(int temp = 0) { m_temp = temp; }
	friend void printWeather(const Temperature& temp, const Humidity& humid);
};

void printWeather(const Temperature& temp, const Humidity& humid)
{
	std::cout << "Temp: " << temp.m_temp << ", Humid: " << humid.m_humid << '\n';
}

int main()
{
	Temperature temp(24);
	Humidity humid(12);
	printWeather(temp, humid);
}