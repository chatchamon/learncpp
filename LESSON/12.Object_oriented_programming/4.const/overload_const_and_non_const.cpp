#include <string>

class Something
{
private:
	std::string m_value;

public:
	Something(const std::string& value = "") { m_value = value; }

	const std::string& getValue() const { return m_value; }
	std::string& getValue() { return m_value; }
};

int main()
{
	Something something;
	something.getValue() = "Hi";

	const Something something2;
	something2.getValue();

	return 0;
}
