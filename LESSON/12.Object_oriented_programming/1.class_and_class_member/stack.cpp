#include <iostream>
#include <array>
#include <cassert>

class Stack
{
private: 
	using container_type = std::array<int, 10>;
	using container_size = std::array<int, 10>::size_type;

private:
	container_type m_array{};
	container_size m_next{0}; // This will hold the index of the next free element on the stack

public:
	void reset()
	{
		m_next = 0;
	}

	bool push(int value)
	{
		if (m_next == m_array.size())
			return false;
		else
		{
			m_array[m_next++] = value;
			return true;
		}
	}

	int pop()
	{
		assert(m_next > 0 && "Cannot pop empty stack");

		return m_array[--m_next];
	}

	void print()
	{
		std::cout << "( ";
		for (int index{ 0 }; index < m_next; ++index)
			std::cout << m_array[index] << ' ';
		std::cout << " )\n";
	}
};

int main()
{
	Stack stack;
	stack.reset();

	stack.print();

	stack.push(5);
	stack.push(3);
	stack.push(8);
	stack.print();

	stack.pop();
	stack.print();

	stack.pop();
	stack.pop();

	stack.print();

	return 0;

	




}