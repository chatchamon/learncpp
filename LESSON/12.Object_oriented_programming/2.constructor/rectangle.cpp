#include <iostream>

class Rectangle
{
private:
	double m_length{ 1.0 };
	double m_width{ 1.0 };

public:
	Rectangle(double length, double width)
		:m_length{ length }, m_width{ width }
	{
	}

	Rectangle(double length)
		:m_length{ length }
	{
	}

	void print()
	{
		std::cout << "L: " << m_length << " W:" << m_width << '\n';
	}
	
};

int main()
{
	Rectangle rect1{ 3.0 };
	rect1.print();

	Rectangle rect2{ 4.0, 5.0 };
	rect2.print();

}