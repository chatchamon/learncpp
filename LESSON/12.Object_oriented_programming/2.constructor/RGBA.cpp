#include <iostream>
#include <cstdint>

class RGBA
{
	using type = std::uint_fast8_t;

private:
	type m_red{};
	type m_green{};
	type m_blue{};
	type m_alpha{};

public:

	RGBA(type red=0, type green=0, type blue=0, type alpha=255)
		:m_red{ red }, m_green{ green }, m_blue{ blue }, m_alpha{alpha}
	{
	}

	void print()
	{
		std::cout << "r=" << static_cast<int>(m_red) << " g=" << static_cast<int>(m_green)
			<< " b=" << static_cast<int>(m_blue) << " a=" << static_cast<int>(m_alpha) << '\n';
	}
};

int main()
{
	RGBA rgba{ 0,127,127};
	rgba.print();
}