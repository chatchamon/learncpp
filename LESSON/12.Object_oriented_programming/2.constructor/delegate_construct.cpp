#include <iostream>
#include <string>

class Employee
{
private:
	int m_id;
	std::string m_name;

public:
	Employee(int id = 0, const std::string& name = "")
	{
		std::cout << "Employee " << m_name << " created.\n";
	}

	//delegate contsructor
	Employee(const std::string& name) : Employee(0, name) {}
};