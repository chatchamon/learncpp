#include <iostream>
#include <cassert>

class Fraction
{
private:
	int m_numerator;
	int m_denominator;

public:
	
	//Redundant
	/*Fraction()
	{
		m_numerator = 0;
		m_denominator = 1;
	}*/

	Fraction(int numerator = 0, int denominator = 1)
	{
		assert(denominator != 0);
		m_numerator = numerator;
		m_denominator = denominator;
	}

	int getNumerator() { return m_numerator; };
	int getDenomenator() { return m_denominator; };
	double getValue() { return static_cast<double>(m_numerator / m_denominator); };
};


int main()
{
	Fraction frac;
	std::cout << frac.getNumerator() << "/" << frac.getDenomenator() << '\n';
	std::cout << frac.getValue() << '\n';

	Fraction zero{};
	std::cout << zero.getNumerator() << "/" << zero.getDenomenator() << '\n';
	std::cout << zero.getValue() << '\n';

	Fraction six{6};
	std::cout << six.getNumerator() << "/" << six.getDenomenator() << '\n';
	std::cout << six.getValue() << '\n';
	
	//this  initialization form should be avoided
	/*Fraction seven = 7;
	std::cout << seven.getNumerator() << "/" << seven.getDenomenator() << '\n';
	std::cout << seven.getValue() << '\n';*/

	Fraction FiveThird{ 5,3 };
	std::cout << FiveThird.getNumerator() << "/" << FiveThird.getDenomenator() << '\n';
	std::cout << FiveThird.getValue() << '\n';

	return 0;
}