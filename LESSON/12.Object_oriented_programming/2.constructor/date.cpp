#include <iostream>

class Date
{
private:
	int m_year{ 2020 };
	int m_month{ 1 };
	int m_day{ 1 };

public:
	Date() = default;

	Date(int year, int month, int day)
	{
		m_year = year;
		m_month = month;
		m_day = day;
	}
};

int main()
{
	Date date{};
	Date today{ 2020,29,4 };

	return 0;

}