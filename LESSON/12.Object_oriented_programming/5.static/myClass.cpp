#include <iostream>
#include <vector>

class MyClass
{
private:
	static std::vector<char> s_mychars;
};

std::vector<char> MyClass::s_mychars{
	[] {
	std::vector<char> v{};

	for (char ch{ 'a' }; ch <= 'z'; ++ch)
		v.push_back(ch);
	return v;
}()
};