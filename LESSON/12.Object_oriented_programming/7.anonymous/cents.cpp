#include <iostream>

class Cents
{
private:
	int m_cents;
public:
	Cents(int cents) { m_cents = cents; }
	int getCents() const { return m_cents; }
};

Cents add(const Cents& cents1, const Cents& censt2)
{
	return Cents(cents1.getCents() + censt2.getCents());
}

int main()
{
	std::cout << "I have " << add(Cents(5), Cents(6)).getCents() << '\n';
}