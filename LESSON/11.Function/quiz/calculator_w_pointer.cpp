#include <iostream>
#include <functional>

int getInteger()
{
	std::cout << "Enter an integer: ";
	int x{};
	std::cin >> x;
	return x;
}

char getOperation()
{
	char op;
	do
	{
		std::cout << "Enter an operation(+,-,*,/): ";
		std::cin >> op;
	} while (op != '+' && op != '-' && op != '*' && op != '/');

	return op;
}

int add(int x, int y)
{
	return x + y;
}

int subtract(int x, int y)
{
	return x - y;
}

int multiply(int x, int y)
{
	return x * y;
}

int divide(int x, int y)
{
	return x / y;
}

using arithmetricFcn = std::function<int(int, int)>;

arithmetricFcn getArithmetricFcn(char op)
{
	switch (op)
	{
	default:
	case'+':return add;
	case'-':return subtract;
	case'*':return multiply;
	case'/':return divide;
	}
}

int main()
{
	int x{ getInteger() };
	int y{ getInteger() };
	char op{ getOperation() };

	arithmetricFcn fcn{ getArithmetricFcn(op) };
	std::cout << x << ' ' << op << ' ' << y << " = " << fcn(x, y) << '\n';

	return 0;
}





