#include <algorithm>
#include <iostream>
#include <functional>

// type alias : example
using validateFcn = bool(*)(int, int);					// alias ver 1
using validatefcn = std::function<bool(int, int)>;		// alias ver 2
bool validate(int x, int y, validateFcn pfcn);


bool ascending(int x, int y)
{
	return x > y;
}

bool descending(int x, int y)
{
	return x < y;
}

bool evensFirst(int x, int y)
{
	if ((x % 2 == 0) && !(y % 2 == 0))
		return false;
	if (!(x % 2 == 0) && (y % 2 == 0))
		return true;

	return ascending(x, y);
}

void selectionSort(int* array, int size, bool (*comparisonFcn)(int, int) = ascending)
{
	for (int startIndex{ 0 }; startIndex < size - 1; ++startIndex)
	{
		int bestIndex{ startIndex };

		for (int currentIndex{ startIndex + 1 }; currentIndex < size; ++currentIndex)
		{
			if (comparisonFcn(array[bestIndex], array[currentIndex]))
				bestIndex = currentIndex;
		}
		std::swap(array[startIndex], array[bestIndex]);
	}
}

void printArray(int* array, int size)
{
	for (int index{ 0 }; index < size; ++index)
		std::cout << array[index] << ' ';
	std::cout << '\n';
}

int main()
{
	int array[9]{ 3,7,9,5,6,1,8,2,4 };

	selectionSort(array, 9, ascending);				// ascending
	printArray(array, 9);

	selectionSort(array, 9, descending);			// descending
	printArray(array, 9);

	selectionSort(array, 9, evensFirst);			// even first
	printArray(array, 9);

	selectionSort(array, 9);						// default case
	printArray(array, 9);

	return 0;
}