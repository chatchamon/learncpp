#include <iostream>
#include <cassert>

int binarySearch(const int* array, int target, int min, int max)
{
	while (min <= max)
	{
		int midpoint = (min+((max - min) / 2));
		if (array[midpoint] < target)
			return binarySearch(array, target, midpoint + 1, max);
		else if (array[midpoint] > target)
			return binarySearch(array, target, min, midpoint - 1);
		else if (array[midpoint] == target)
			return midpoint;
	}
	return -1;
}


int main()
{
	constexpr int array[]{ 3, 6, 8, 12, 14, 17, 20, 21, 26, 32, 36, 37, 42, 44, 48 };
	constexpr int numTestValue{ 9 };
	constexpr int testValues[numTestValue]{ 0,3,12,13,22,26,43,44,49 };
	int expectedValues[numTestValue]{ -1, 0, 3, -1, -1, 8, -1, 13, -1 };

	for (int i{ 0 }; i < numTestValue; ++i)
	{
		int index{ binarySearch(array, testValues[i],0,14) };
		if (index == expectedValues[i])
			std::cout << "test value " << testValues[i] << " passed\n";
		else
			std::cout << "test value " << testValues[i] << " failed\n";
	}

	return 0;
}