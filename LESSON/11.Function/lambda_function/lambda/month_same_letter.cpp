#include <iostream>
#include <array>
#include <string_view>

int main()
{
	//std::array<const char*, 12> months{
	std::array months{
		"January", "February", "March", "April", "May", "June",
			"July", "August", "September", "October", "November", "September"
	};

	auto sameLetter{ std::adjacent_find(months.begin(), months.end(),
										[](const auto& a, const auto& b)
										{
											return(a[0] == b[0]);
										})};

	auto fiveLetterMonths{ std::count_if(months.begin(), months.end(),
							[](std::string_view str) {
								return (str.length() == 5);
							}) };
	
	if (sameLetter != months.end())
	{
		std::cout << *sameLetter << " and " << *std::next(sameLetter)
			<< " start with the same letter\n";
	}

	std::cout << "There are " << fiveLetterMonths << " months with 5 letters\n";

	return 0;
}