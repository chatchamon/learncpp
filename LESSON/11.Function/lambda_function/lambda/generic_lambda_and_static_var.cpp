#include <iostream>
#include <array>

int main()
{
	auto print{
		[](auto value) {
			static int callCount{0};
			std::cout << callCount++ << " : " << value << '\n';
		} };

	print("Hello");
	print("World");

	print(1);
	print(2);

	print("ding dong");

	return 0;

}