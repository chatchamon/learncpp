#include <iostream>
#include <string_view>
#include <array>
#include <algorithm>

int main()
{
	std::array<std::string_view, 4> arr{ "apple", "banana", "walnut", "lemon" };

	auto found{ std::find_if(arr.begin(), arr.end(),
				[](std::string_view str)
				{
					return (str.find("nut") != std::string_view::npos);
				}) };

	if (found == arr.end())
		std::cout << "No nuts\n";
	else
		std::cout << "Found " << *found << '\n';

	return 0;
}
	