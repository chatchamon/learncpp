#include <iostream>

int main()
{
	auto divide{ [](int x, int y, bool bInteger) -> double {
		if (bInteger)
			return x / y;
		else
			return static_cast<double>(x) / y;
	} };

	std::cout << divide(3, 2, true) << '\n';
	std::cout << divide(3, 2, false) << '\n';

	return 0;

}