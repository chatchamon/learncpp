auto isEven{
	[](int i)
	{
		return ((i % 2) == 0);
	}
};

return std::all_of(array.begin(), array.end(), isEven);