#include <functional>
#include <iostream>

int main()
{
	double(*addNumber1)(double, double) {
		[](double a, double b) {
			return a + b;
		}
	};

	std::cout << addNumber1(1, 2) << '\n';

	std::function addNumber2{
		[](double a, double b) {
			return a + b;
		}
	};

	std::cout << addNumber2(3, 4) << '\n';

	auto addNumber3{
		[](double a, double b) {
		return a + b;
		}
	};

	std::cout << addNumber3(5, 6) << '\n';

	return 0;
}