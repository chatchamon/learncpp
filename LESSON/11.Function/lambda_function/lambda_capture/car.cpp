#include <string>
#include <iostream>
#include <array>
#include <algorithm>

struct Car
{
	std::string make{};
	std::string model{};
};

int main()
{
	std::array<Car, 3> cars{ {{"Volswagen", "Golf"},{"Toyota", "Corolla"},{"Honda", "Civic"}} };

	int comparison{0};

	std::sort(cars.begin(), cars.end(),
		[&comparison](const auto& a, const auto& b)
		{
			++comparison;
			return (a.make < b.make);
		});
	
	std::cout << "Comparison: " << comparison << '\n';
		
	for (const auto& car : cars)
		std::cout << car.make << ' ' << car.model << '\n';

	return 0;
}