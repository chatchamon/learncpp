#include <iostream>
#include <vector>
#include <ctime>
#include <random>
#include <cmath>		// std::abs
#include <algorithm>	// std::find, std::min_element

int multiplier()
{
	static std::mt19937 mt{ static_cast<std::mt19937::result_type>(std::time(nullptr)) };
	return std::uniform_int_distribution{ 2,4 }(mt);
}

std::vector<int> getList(int start, int count, int multiply)
{
	int index{ start };
	std::vector<int> numbers(static_cast<std::vector<int>::size_type>(count));

	for (auto& number : numbers)
	{
		number = index * index * multiply;
		index++;
	}
	return numbers;
}

std::vector<int> getUserNum(int multiply)
{
	std::cout << "Start where? ";
	int start{};
	std::cin >> start;

	std::cout << "How many? ";
	int count{};
	std::cin >> count;

	return getList(start, count, multiply);
}

void printList(std::vector<int> numbers)	// optional function
{
	for (auto& element : numbers)
		std::cout << element << ' ';
	std::cout << '\n';
}

void printTask(std::vector<int>::size_type count, int multiplier)
{
	std::cout << "I generated " << count << " square numbers.";
	std::cout << " Do you know what each number is after multiplying by " << multiplier;
	std::cout << '\n';
}

int getGuess()
{
	std::cout << "> ";
	int guess{};
	std::cin >> guess;
	return guess;
}

bool checkGuess(std::vector<int>& numbers, int guess)
{
	auto found{ std::find(numbers.begin(), numbers.end(), guess) };
	if(found == numbers.end())
	{
		return false;
	}
	else {
		numbers.erase(found);
		return true;
	}
}

void printSuccess(std::vector<int>::size_type numberLeft)
{
	if (numberLeft == 0) {
		std::cout << "Nice! You found all numbers, good job!\n";
	}
	else {
		std::cout << "Nice! " << numberLeft << " number(s) left.\n";
	}
}

int correctAnswer(std::vector<int>& numbers, int guess)
{
	return *std::min_element(numbers.begin(), numbers.end(), [&guess](int a, int b) {
		return (std::abs(a - guess) < std::abs(b - guess));
		});
		
}

void printFail(std::vector<int>& numbers, int guess)
{
	std::cout << guess << " is wrong! Try " << correctAnswer(numbers, guess) << " next time.\n";
}

bool play(std::vector<int>& numbers)
{
	int guess{ getGuess() };
	if (checkGuess(numbers, guess))
	{
		printSuccess(numbers.size());
		return !numbers.empty();
	}
	else
	{
		printFail(numbers,guess);
		return false;
	}
}
	

int main()
{
	int multiply{ multiplier() };
	
	std::vector<int> a = getUserNum(multiply);
printList(a);
	printTask(a.size(), multiply);

	while (play(a))
		;

	return 0;
	
}