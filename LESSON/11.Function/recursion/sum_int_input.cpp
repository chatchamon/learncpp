#include <iostream>

int sumIntInput(int x)
{
	if (x < 10)
	{
		std::cout <<"x = " << x << '\n';
		return x;
	}
	else
	{
		std::cout << "x % 10 = " << x % 10 << '\n';
		std::cout << "x / 10 = " << x / 10 << '\n';
		return sumIntInput(x / 10) + x % 10;
	}
}

int main()
{
	std::cout << "Enter an input: ";
	int x{};
	std::cin >> x;
	std::cout << sumIntInput(x) << '\n';

}
