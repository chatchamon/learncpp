#include <iostream>

void countDown(int x)
{
	std::cout << "push " << x << '\n';
	countDown(x - 1);
}

int main()
{
	countDown(5);

	return 0;
}