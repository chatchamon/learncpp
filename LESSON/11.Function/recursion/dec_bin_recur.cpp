#include <iostream>
#include <vector>

//actually we can combine both positive and negative integer to one case, by using unsigned int
//and we should have another function for checking input to which case (pos + neg / zero)
void printDecToBin(int x)
{
	std::vector<int> stack;
	if (x < 1)
		return;
	else
	{
		stack.push_back(x % 2);
		printDecToBin(x / 2);
	}
	
	for (auto element : stack)
		std::cout << element << ' ';
}

void printZero()
{
	std::cout << "0000'0000" << '\n';
}

void printNeg(unsigned int x)
{
	if (x > 1)
		printNeg(x / 2);
	std::cout << x % 2;
}

int main()
{
	std::cout << "Enter an input: ";
	int x{};
	std::cin >> x;
	if (x == 0)
		printZero();
	else if (x > 0)
		printDecToBin(x);
	else if (x < 0)
		printNeg(x);

	return 0;

}