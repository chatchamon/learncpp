#include <cassert>
#include <array>

int getArrayValue(const std::array<int, 10>& array, int index)
{
	assert(index >= 0 && index <= 9);

	return array[index];
}

int main()
{
	std::array<int, 10> array = { 1,2,3,4,5,6,7,8,9,10 };
	getArrayValue(array, -3);
}