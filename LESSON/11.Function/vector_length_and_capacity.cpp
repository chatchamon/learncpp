#include <iostream>
#include <vector>

int main()
{
	std::vector<int> array;
	array = { 0,1,2,3,4 };
	std::cout << "array length: " << array.size() << " , array capacity: " << array.capacity() << '\n';

	array = { 9,8,7 };
	std::cout << "array length: " << array.size() << " , array capacity: " << array.capacity() << '\n';

	return 0;

}