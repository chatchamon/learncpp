#include <iostream>
#include <cstdarg>		// for elipsis

double findAverage(int count, ...)
{
	double sum = 0;

	//access elipsis through va_list
	va_list list;

	va_start(list, count);

	for (int arg = 0; arg < count; ++arg)
		sum += va_arg(list, int);

	va_end(list);

	return sum / count;
}

int main()
{
	std::cout << findAverage(5, 1, 2, 3, 4, 5) << '\n';
	std::cout << findAverage(6, 1, 2, 3, 4, 5, 6) << '\n';
}