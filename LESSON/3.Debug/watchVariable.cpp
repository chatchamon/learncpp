#include <iostream>

int main()
{
	int x{ 1 };
	int y{ 2 }; 
	std::cout << x << " ";
	std::cout << y << " ";

	x = x + 2;
	std::cout << x << " ";

	x = x + 3;
	std::cout << x << " ";

	return 0;
}