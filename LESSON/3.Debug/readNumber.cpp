#include <iostream>

int readNumber()
{
	int a{};
	std::cout << "Please enter a number: ";
	std::cin >> a;
	return a;
}

void writeAnswer(int x)
{
	std::cout << "The sum is:" << x;
}

int main()
{
	int x{ readNumber() };
	x = x + readNumber();
	writeAnswer(x);

	return 0;
}