#include <iostream>
#include "operation.h"

int getUserInput()
{
	std::cout << ("Enter an integer: ");
	int value{};
	std::cin >> value;
	return value;
}

int addInt(int a, int b)
{
	return a + b;
}

void printResult(int c)
{
	std::cout << "adding two integers is " << c << '\n';
//std::cerr << "printResult() called\n";
}