#include <iostream> // input, output
#include <cstdint> //uint_fastx_t

int main()
{
	std::uint_fast32_t redBits{   0xff000000 };
	std::uint_fast32_t greenBits{ 0x00ff0000 };
	std::uint_fast32_t blueBits{  0x0000ff00 };
	std::uint_fast32_t alphaBits{ 0x000000ff };
//std::cerr << std::hex << redBits << '\n';

	std::cout << "Enter a 32-bit RGBA color value in hexadecimal (e.g. FF7F3300):";
	std::uint_fast32_t pixel{};
	std::cin >> std::hex >> pixel;
//std::cerr << "you enter " << std::hex <<  pixel << '\n';

//we receive value in fast32_t, but we need (red = ff) not (red = ff000000)
//so, we need to shift to position 8(87654321) 
//so we set uint_fast8_t not fast32_t
//it is unsigned, to show we ned to change it to signed >> we use static_cast<int>(xx)
	std::uint_fast8_t red = (pixel&redBits) >> 24 ;
std::cerr << "red = "  << std::hex << static_cast<int>(red);
	std::uint_fast8_t green = (pixel & greenBits) >> 16;
	std::uint_fast8_t blue = (pixel & blueBits) >> 8;
	std::uint_fast8_t alpha = (pixel & alphaBits);

	std::cout << "Your color contains: " << '\n';
	std::cout << std::hex;
	std::cout << "red = " <<  static_cast<int>(red) << '\n';
	std::cout << "green = "  << static_cast<int>(green) <<'\n';
	std::cout << "blue = "  << static_cast<int>(blue) <<'\n';
	std::cout << "alpha = "  << static_cast<int>(alpha) <<'\n';

	return 0;


}