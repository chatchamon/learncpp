#include <iostream> //printing
#include <cstdint> //unsigned int type c11++

int main()
{
	constexpr std::uint_fast8_t isHungry{   1 << 0 };
	constexpr std::uint_fast8_t isSad{      1 << 1 };
	constexpr std::uint_fast8_t isMad{      1 << 2 };
	constexpr std::uint_fast8_t isHappy{    1 << 3 };
	constexpr std::uint_fast8_t isLaughing{ 1 << 4 };
	constexpr std::uint_fast8_t isAsleep{   1 << 5 };
	constexpr std::uint_fast8_t isDead{     1 << 6 };
	constexpr std::uint_fast8_t isCrying{   1 << 7 };
	
	std::uint_fast8_t me{};
	me |= (isHappy | isLaughing);
	me &= ~(isLaughing);

	std::cout << "I am happy? " << static_cast<bool>(me & isHappy) <<'\n';
	std::cout << "I am laughing? " << static_cast<bool>(me&isLaughing)<<'\n';



	return 0;
}
