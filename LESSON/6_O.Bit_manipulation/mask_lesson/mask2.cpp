#include <iostream>
#include <bitset>

int main()
{
	std::uint_fast8_t mask0{ 0b0000'0001 };
	std::uint_fast8_t mask1{ 0b0000'0010 };
	std::uint_fast8_t mask2{ 0b0000'0100 };
	std::uint_fast8_t mask3{ 0b0000'1000 };
	std::uint_fast8_t mask4{ 0b0001'0000 };
	std::uint_fast8_t mask5{ 0b0010'0000 };
	std::uint_fast8_t mask6{ 0b0100'0000 };
	std::uint_fast8_t mask7{ 0b1000'0000 };

	std::bitset<8> flags{ 0b0000'0101 };

	std::cout << "Flag: " << flags << '\n';
	//std::cout << "Then, bit 1 is " << flags.test(1)
		//<< "and bit 2 is " << flags.test(2) << '\n';

	//flip bit 1 and 2
	flags ^= (mask1 | mask2);
	std::cout << "Flipping bit 1 and 2 \n";
	std::cout << ((flags.test(1)) ? "1 is on\n" : "1 is off\n")
		<< ((flags.test(2) ? "2 is on\n" : "2 is off\n")) << '\n';

	//turn bit 1 and 2 on
	flags |= (mask1 | mask2);
	std::cout << "turning bit 1 and 2 on\n";
	std::cout << ((flags.test(1)) ? "1 is on\n" : "1 is off\n")
		<< ((flags.test(2) ? "2 is on\n" : "2 is off\n")) << '\n';

	//turn bit 1 and 2 off
	flags &= ~(mask1 | mask2);
	std::cout << "turning bit 1 and 2 off\n";
	std::cout << ((flags.test(1)) ? "1 is on\n" : "1 is off\n")
		<< ((flags.test(2) ? "2 is on\n" : "2 is off\n")) << '\n';

	return 0;
}