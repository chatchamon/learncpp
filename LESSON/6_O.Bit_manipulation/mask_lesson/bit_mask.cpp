#include <iostream>
#include <bitset>
#include <cstdint>

int main()
{
	constexpr std::bitset<8> mask0{ 0b0000'0001 };
	constexpr std::bitset<8> mask1{ 0b0000'0010 };
	constexpr std::bitset<8> mask2{ 0b0000'0100 };
	constexpr std::bitset<8> mask3{ 0b0000'1000 };
	constexpr std::bitset<8> mask4{ 0b0001'0000 };
	constexpr std::bitset<8> mask5{ 0b0010'0000 };
	constexpr std::bitset<8> mask6{ 0b0100'0000 };
	constexpr std::bitset<8> mask7{ 0b1000'0000 };

	std::bitset<8> flags{ 0b0000'0101 };

	std::cout << "intial status: " << flags << '\n';
	std::cout << "bit 1: " << flags.test(1) << " \nbit 2: " << flags.test(2) << '\n';

	flags ^= (mask1 | mask2);
	std::cout << "flipping bit 1 and 2\n";
	std::cout << "bit 1: " << flags.test(1) << " \nbit 2: " << flags.test(2) << '\n';

	flags |= (mask1 | mask2);
	std::cout << "turning bit 1 and 2 on\n";
	std::cout << "bit 1: " << flags.test(1) << " \nbit 2: " << flags.test(2) << '\n';

	flags &= ~(mask1 | mask2);
	std::cout << "turning bit 1 and 2 off\n";
	std::cout << "bit 1: " << flags.test(1) << " \nbit 2: " << flags.test(2) << '\n';




	return 0;

}