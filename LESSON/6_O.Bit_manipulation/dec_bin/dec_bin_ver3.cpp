#include <iostream>

int printAndDecrementOne(int input, int pow)
{
    std::cout << '1';
    return (input - pow);
}

int printAndDecrementBit(int input, int pow)
{
    if (input >= pow)
        return printAndDecrementOne(input, pow);

    std::cout << '0';
    return input;
}

int main()
{
    std::cout << "Enter an integer between 0 and 255: ";
    int x{};
    std::cin >> x;

    x = printAndDecrementBit(x, 128);
    x = printAndDecrementBit(x, 64);
    x = printAndDecrementBit(x, 32);
    x = printAndDecrementBit(x, 16);

    std::cout << ' ';

    x = printAndDecrementBit(x, 8);
    x = printAndDecrementBit(x, 4);
    x = printAndDecrementBit(x, 2);
    x = printAndDecrementBit(x, 1);

    std::cout << '\n';

}

