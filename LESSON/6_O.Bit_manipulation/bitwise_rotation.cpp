#include <iostream>
#include <bitset>

//Bitwise rotation is like a shift left with an extra step.
//It will check the intial value in position 3 : bit.test(3).
//If bit.test(3) == 1, 1 will be added up after shifting left
//ex 1001, shifed left 0010, rotation left 0011

std::bitset<4> rotl(std::bitset<4> bits)
{
	int leftbit = bits.test(3);
	bits <<= 1;

	if (leftbit == 1)
		bits.set(0);
	return bits;

	//other method when you don't use bit.test and bit.set
	//return (bits<<1) | (bits >>3);

int main()
{
	std::bitset<4> bits1{ 0b0001 };
	std::cout << rotl(bits1) << '\n';

	std::bitset<4> bits2{ 0b1001 };
	std::cout << rotl(bits2) << '\n';

	return 0;
}