#include <iostream>
#include <bitset>

int main()
{
	std::bitset<4> x{ 0b1100 };

	std::cout << x << 1 << '\n';		// print x then 1
	std::cout << (x << 1) << '\n';		// shiftleft x by 1

	return 0;

}