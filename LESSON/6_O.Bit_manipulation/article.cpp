#include <cstdint>
#include <iostream>

int main()
{
    constexpr std::uint_fast8_t option_viewed{ 0x01 };
    constexpr std::uint_fast8_t option_edited{ 0x02 };
    constexpr std::uint_fast8_t option_favorited{ 0x04 };
    constexpr std::uint_fast8_t option_shared{ 0x08 };
    constexpr std::uint_fast8_t option_deleted{ 0x80 };

    std::uint_fast8_t myArticleFlags{};

    //set the article as viewed
    myArticleFlags |= option_viewed;
    std::cout << std::hex << static_cast<int>(myArticleFlags) << '\n';

    //check if the article was deleted
    if (myArticleFlags & option_deleted)
        std::cout << "It is deleted\n";
    else
        std::cout << "It is not deleted\n";

    //clear the article as a favorite
    myArticleFlags &= ~(option_favorited);
    std::cout << std::hex << static_cast<int>(myArticleFlags) << '\n';








    return 0;
}