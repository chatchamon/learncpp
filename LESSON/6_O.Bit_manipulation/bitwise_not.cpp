#include <iostream>
#include <bitset>

int main()
{
	std::cout << std::bitset<4>{~0b0100u} << ' ' << std::bitset<8>{~0b0100u} << '\n';					//bitwise_not

	std::cout << (std::bitset<4>{0b0101} | std::bitset<4>{0b0110}) << '\n';								//bitwise_or (2)

	std::cout << (std::bitset<4>{0b0111} | std::bitset<4>{0b0011} | std::bitset<4>{0b0001}) << '\n';	//bitwise_or (3)

	std::cout << (std::bitset<4>{0b0101} & std::bitset<4>{0b0110});										//bitwise_and (2)

	std::cout << (std::bitset<4>{0b0001} &std::bitset<4>{0b0011} &std::bitset<4>{0b0111}) << '\n';		//bitwise_and (3)

	std::bitset<4> bits{ 0b0100 };																		//assign shift_left							
	bits >>= 1;
	std::cout << bits << '\n';

	return 0;
}