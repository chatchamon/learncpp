#include<iostream>

struct Point3d
{
	double x;
	double y;
	double z;
};

Point3d getZeroPt()
{
	Point3d temp{ 0.0, 0.0, 0.0 };
	return temp;
}

Point3d getZeroPt2()
{
	return { 0.0, 0.0, 0.0 };
}

Point3d getZeroPt3()
{
	return {};
}

int main()
{
	//Point3d zero{ getZeroPt() };
	//Point3d zero{ getZeroPt2() };
	Point3d zero{ getZeroPt3() };
	if (zero.x == 0.0 && zero.y == 0.0 && zero.z == 0.0)
		std::cout << "The point is zero\n";
	else
		std::cout << "The point is not zero\n";

	return 0;
}