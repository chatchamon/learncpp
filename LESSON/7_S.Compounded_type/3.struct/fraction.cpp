#include <iostream>

struct Fraction
{
	double numerator;
	double denominator;
};

Fraction getFraction()
{
	Fraction temp;
	std::cout << "Enter a fraction: ";
	std::cin >> temp.numerator;
	std::cin.ignore(32767, '/');
	std::cin >> temp.denominator;
	return temp;
}

double multiplyFraction(Fraction x, Fraction y)
{
	double mulNumer{ x.numerator * y.numerator };
	double mulDeno{ x.denominator * y.denominator };

	double result{ mulNumer / mulDeno };
	return result;
}

int main()
{
	Fraction a{ getFraction() };
	Fraction b{ getFraction() };
	std::cout << std::endl;

	std::cout << "Multiply two fraction: " << '\n';
	std::cout << "Result is: " << multiplyFraction(a, b) << '\n';

	return 0;
}