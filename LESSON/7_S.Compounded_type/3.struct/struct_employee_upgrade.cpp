#include <iostream>

struct Employee
{
	short id;
	int age;
	double wage;
};

void printInfo(Employee employee)
{
	std::cout << "ID:	" << employee.id << '\n';
	std::cout << "Age:	" << employee.age << '\n';
	std::cout << "Wage:	" << employee.wage << '\n';
}

int main()
{
	Employee imm{ 12, 23, 8700.0 };
	Employee or_jeeez{ 21, 26, 190'000.0 };

	printInfo(imm);
	std::cout << '\n';
	printInfo(or_jeeez);

	return 0;
}