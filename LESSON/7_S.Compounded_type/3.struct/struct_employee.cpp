#include<iostream>

struct Employee						//when construct struct, don't forget (;) after each member
{
	short id;
	int age;
	double wage{ 12'000.0 };		//intialize:in case some don't specify, they will have initial value 
};

struct Rectangular
{
	double width{ 1.0 };
	double length{ 1.0 };
};

int main()
{
//Employee struct
	Employee imm;							// 1st way of assessing
	imm.id = 12;
	imm.age = 23;
	imm.wage = 8700;

	Employee or_jeeez{21, 26, 190000.0};			//2nd way of assessing

	int totalAge{ static_cast<int>(imm.wage + or_jeeez.wage) };
	if (imm.wage > or_jeeez.wage)
		std::cout << "Imm makes more than Or\n";
	else if (imm.wage == or_jeeez.wage)
		std::cout << "Imm makes equal to Or\n";
	else
		std::cout << "Imm makes less than Or\n";

	//Imm get a promotion
	imm.wage += 1000;

	//This year is or_jeeez's birthday
	or_jeeez.age += 1;

//Rectangular struct
	Rectangular x;
	std::cout << "x width " << x.width << " x length " << x.length << '\n';

	x.length = 2.0;
	std::cout << "x width " << x.width << " x length " << x.length << '\n';

	Rectangular y{ 3.0, 3.0 };
	std::cout << "y width " << y.width << " y length " << y.length << '\n';

	return 0;
}