#include <iostream>

struct Employee
{
	short id;
	int age;
	double wage;
};

struct Company		//nested struct
{
	Employee CEO;
	int numberOfEmployees;
};

int main()
{
	Company mycompany{ {1, 45, 120'000.0}, 7 };
	
	std::cout << "CEO's wage: " << mycompany.CEO.wage << '\n';

	return 0;
}