#ifndef MONSTER_H
#define MONSTER_H

#include <string>

enum class MonsterType
{
	ORGE,
	DRAGON,
};

struct Monster
{
	MonsterType type;
	std::string name;
	int health;
};

namespace monsters
{
	std::string getMonsterType(Monster m);
	void printMonster(Monster m);
}


#endif 