#include "monster.h"
#include <string>
#include <iostream>

namespace monsters
{
	std::string getMonsterType(Monster m)
	{
		if (m.type == MonsterType::ORGE)
			return "Orge";
	}

	void printMonster(Monster m)
	{
		std::cout << "This " << getMonsterType(m) << " is named " << m.name
			<< " and has " << m.health << " health.\n";
	}
}
