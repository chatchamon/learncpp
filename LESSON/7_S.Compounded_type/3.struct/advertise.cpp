#include<iostream>

struct Advertise
{
	int numAd;
	double percentUserClickAd;
	double earning;
};

Advertise getAd()
{
	Advertise temp;
	std::cout << "How many adversitise show to users? : ";
	std::cin >> temp.numAd;
	std::cout << "How much percentage of ads were clicked by users? : ";
	std::cin >> temp.percentUserClickAd;
	std::cout << "How much earning on this ad?: ";
	std::cin >> temp.earning;
	std::cout << '\n';

	return temp;
}

void printAd(Advertise advertise)
{
	std::cout << "Number of advertise: " << advertise.numAd << " pieces" << '\n' ;
	std::cout << "Percentaged clicked by users: " << advertise.percentUserClickAd << " %" << '\n';
	std::cout << "Earning: " << advertise.earning << " $." << '\n';
}

int main()
{
	Advertise x{ getAd() };
	printAd(x);
	
	double totalEarning = x.numAd * (x.percentUserClickAd / 100) * x.earning;
	std::cout << "Total Earning: " << totalEarning << " $" << '\n';

	return 0;
}