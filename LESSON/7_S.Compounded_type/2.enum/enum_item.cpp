#include <iostream>
#include <string>

enum ItemType
{
	ITEMTYPE_SWORD,
	ITEMTYPE_TORCH,
	ITEMTYPE_POTION,
};

std::string getItemName(ItemType itemtype)
{
	if (itemtype == ITEMTYPE_SWORD)
		return "Sword";
	if(itemtype == ITEMTYPE_TORCH)
		return "Torch";
	if (itemtype == ITEMTYPE_POTION)
		return "Potion";
}

int main()
{
	ItemType itemType = ITEMTYPE_TORCH;

	std::cout << "You carry a ";
	std::cout << getItemName(itemType) << '\n';

	return 0;
}