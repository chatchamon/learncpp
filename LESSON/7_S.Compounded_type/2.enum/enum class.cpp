#include <iostream>

int main()
{
	enum class Color
	{
		RED,
		BLUE
	};

	enum class Fruit
	{
		BANANA,
		APPLE,
	};

	Color color = Color::RED;
	Fruit fruit = Fruit::BANANA;

	/*
	if (color == fruit)			// not gonna work, as they come from dif enum class
		std::cout << "color and fruit are equal\n";
	else
		std::cout << "color and fruit are not equal\n"; 
	*/

	/*
	if (color == Color::RED)
		std::cout << "The color is Red!\n";
	else if (color == Color::BLUE)
		std::cout << "The color is Blue!\n";
	*/

	/*
	//std::cout << color << '\n';			//won't work
	std::cout << static_cast<int>(color) << '\n';
	*/

	return 0;
}

