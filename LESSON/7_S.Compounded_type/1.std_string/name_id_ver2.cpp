#include <iostream>
#include <string>

int getAndCheckNum()
{
	std::cout << "Enter a number (1 or 2): ";
	int num{};
	std::cin >> num;

	while ((num != 1) && (num != 2))
	{
		std::cout << "enter again! 1 or 2";
		std::cerr << "your current num: " << num << '\n';
		std::cout << "Enter a number (1 or 2): ";
		std::cin >> num;
	}

	std::cout << std::endl;
	return num;
}

std::string getName()
{
	std::cout << "Now enter your name: ";
	std::string name{};
	std::getline(std::cin, name);

	return name;
}

int main()
{
	int a{ getAndCheckNum() };

	std::cin.ignore(32767, '\n');	//ignore up to 32767 characters until a \n is removed
	std::string fullname{ getName() };

	std::cout << "Hello " << fullname << " , you picked " << a << '\n';

	return 0;
}