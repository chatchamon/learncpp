#include <iostream>
#include <string>

int main()
{
	std::cout << "Enter a number (1 or 2): ";
	int x{};
	std::cin >> x;

	if (x != (1 || 2))
	{
		std::cout << "enter again! only 1 or 2\n";
		std::cout << "Enter a number (1 or 2): ";
		std::cin >> x;
	}

	std::cin.ignore(32767, '\n');

	std::cout << "Enter your name: ";
	std::string name{};
	std::getline(std::cin, name);

	std::cout << "Hello " << name << ", you picked " << x << '\n';

	return 0;
}