#include <iostream>
#include <string>

int main()
{
	std::string a{ "45" };
	std::string b{ "11" };

	//concatenation not sum up
	std::cout << a + b << '\n';
	a += " volts";
	std::cout << a << '\n';

	//ask for length
	std::cout << "a+b has length as " << (a + b).length() << " charatcers\n"; //45 volts11

	return 0;
}