#include <iostream>
#include <string>

int main()
{
	std::cout << "Enter your full name: ";
	std::string name{};
	std::getline(std::cin, name);

	std::cout << "Enter your age: ";
	int age{};
	std::cin >> age;
	std::cin.ignore(32767, '\n');

//std::cerr << "age: " << age << " name.length: " << name.length() << '\n';

	double agePerName = static_cast<double>(age) / name.length();

	std::cout << "You've lived " << agePerName << " years for each letter in your name.";

	return 0;
}